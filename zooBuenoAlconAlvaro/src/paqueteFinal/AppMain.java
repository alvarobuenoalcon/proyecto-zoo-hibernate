package paqueteFinal;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import hibernate.Animal;
import hibernate.Empleado;
import hibernate.Entrada;
import hibernate.Especie;
import hibernate.Evento;
import hibernate.Tratamiento;
import hibernate.Zona;

import javax.sound.midi.MidiDevice.Info;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;

public class AppMain extends JFrame {
	
	static Persistencia per = null;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppMain frame = new AppMain();
					inicio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected static void inicio() {
				
				// Cargamos CFG.INI
				try {
					Properties prop = new Properties();
					prop.load(new InputStreamReader(AppMain.class.getResourceAsStream("CFG.INI"))); // CFG.INI se buscará en el
																									// mismo paquete que la clase
																									// AppMain
					String tipoPersistencia = prop.getProperty("tipoPersistencia");
					switch (tipoPersistencia) {
					case "mysqlJDBC":
						String servidor = prop.getProperty("mysqlJDBC.servidor");
						String baseDatos = prop.getProperty("mysqlJDBC.basedatos");
						String puerto = prop.getProperty("mysqlJDBC.puerto");
						String usuario = prop.getProperty("mysqlJDBC.usuario");
						String password = prop.getProperty("mysqlJDBC.password");
						// per = new PersistenciaMySQL(servidor, puerto, baseDatos, usuario, password);
						break;
					case "hibernate":
						try {
							per = new PersistenciaHibernate(prop.getProperty("hibernate.archivoCFG"), true);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, "El archivo 'CFG.INI' esta da�ado o no existe.");
							System.exit(-1);
						}
						break;
					default:
						throw new IllegalArgumentException(tipoPersistencia + ": tipo de persistencia no soportada");
					}
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "El archivo 'CFG.INI' esta da�ado o no existe.");
					System.exit(-1);
					e.printStackTrace();
				}
		
	}

	/**
	 * Create the frame.
	 */
	public AppMain() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 609, 466);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnZonas = new JButton("Zonas");
		btnZonas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				abrirZona();
			}
		});
		btnZonas.setBounds(10, 49, 89, 23);
		contentPane.add(btnZonas);
		
		JButton btnEspecies = new JButton("Especies");
		btnEspecies.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirEspecies();
			}
		});
		btnEspecies.setBounds(123, 49, 89, 23);
		contentPane.add(btnEspecies);
		
		JButton btnAlimentos = new JButton("Alimentos");
		btnAlimentos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirAlimentos();
			}
		});
		btnAlimentos.setBounds(236, 49, 123, 23);
		contentPane.add(btnAlimentos);
		
		JLabel lblMantenimientos = new JLabel("MANTENIMIENTOS");
		lblMantenimientos.setBounds(10, 11, 140, 14);
		contentPane.add(lblMantenimientos);
		
		JButton btnTratamientos = new JButton("Tratamientos");
		btnTratamientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirTratamientos();
			}
		});
		btnTratamientos.setBounds(382, 49, 123, 23);
		contentPane.add(btnTratamientos);
		
		JButton btnAnimales = new JButton("Animales");
		btnAnimales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirAnimales();
			}
		});
		btnAnimales.setBounds(10, 83, 89, 23);
		contentPane.add(btnAnimales);
		
		JButton buttonEmpleados = new JButton("Empleados");
		buttonEmpleados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrarEmpleados();
			}
		});
		buttonEmpleados.setBounds(236, 83, 123, 23);
		contentPane.add(buttonEmpleados);
		
		JButton btnEventos = new JButton("Eventos");
		btnEventos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirEventos();
			}
		});
		btnEventos.setBounds(123, 83, 89, 23);
		contentPane.add(btnEventos);
		
		JLabel lblOtrosProcesos = new JLabel("OTROS PROCESOS");
		lblOtrosProcesos.setBounds(10, 133, 140, 14);
		contentPane.add(lblOtrosProcesos);
		
		JButton btnEntradas = new JButton("Entradas");
		btnEntradas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				abrirEntradas();
			}
		});
		btnEntradas.setBounds(330, 226, 202, 23);
		contentPane.add(btnEntradas);
		
		JButton buttonTrabajadoresXZona = new JButton("Trabajadores por zona");
		buttonTrabajadoresXZona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				abrirTrabPorZona();
			}
		});
		buttonTrabajadoresXZona.setBounds(10, 158, 202, 23);
		contentPane.add(buttonTrabajadoresXZona);
		
		JButton btnEmisionDeNominas = new JButton("Emision de nominas");
		btnEmisionDeNominas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirNominas();
			}
		});
		btnEmisionDeNominas.setBounds(10, 192, 202, 23);
		contentPane.add(btnEmisionDeNominas);
		
		JButton btnCapacitacionDeEmpleados = new JButton("Capacitacion de empleados");
		btnCapacitacionDeEmpleados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirCapacitEmpleados();
			}
		});
		btnCapacitacionDeEmpleados.setBounds(10, 226, 202, 23);
		contentPane.add(btnCapacitacionDeEmpleados);
		
		JButton buttonConsumoAlimento = new JButton("Consumo de alimentos");
		buttonConsumoAlimento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirConsAlim();
			}
		});
		buttonConsumoAlimento.setBounds(330, 158, 202, 23);
		contentPane.add(buttonConsumoAlimento);
		
		JButton btnTratamientosPorAnimal = new JButton("Tratamientos por animal");
		btnTratamientosPorAnimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirTratPorAnim();
			}
		});
		btnTratamientosPorAnimal.setBounds(330, 192, 202, 23);
		contentPane.add(btnTratamientosPorAnimal);
		
		JLabel lblInformes = new JLabel("INFORMES");
		lblInformes.setBounds(10, 267, 140, 14);
		contentPane.add(lblInformes);
		
		JButton btnListadoDeTratamientos = new JButton("Listado de tratamientos");
		btnListadoDeTratamientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirInforme();
			}
		});
		btnListadoDeTratamientos.setBounds(10, 292, 202, 23);
		contentPane.add(btnListadoDeTratamientos);
	}

	protected void abrirInforme() {
		List<Animal> al23 = per.extraerAnimal("","");
		if (al23.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay animales disponibles.");
			return;
		}
		Informe inf = new Informe(this);
		setEnabled(false);
		inf.setVisible(true);
	}

	protected void abrirEntradas() {
		List<Evento> al2 = per.extraerEvento("");
		if (al2.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay eventos disponibles.");
			return;
		}
		
		EntradasGUI en= new EntradasGUI(this);
		setEnabled(false);
		en.setVisible(true);
	}

	protected void abrirTratPorAnim() {
		List<Animal> al23 = per.extraerAnimal("","");
		if (al23.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay animales disponibles.");
			return;
		}
		List<Empleado> al2 = per.extraerEmpleado("", "");
		if (al2.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay empleados disponibles.");
			return;
		}
		List<Tratamiento> al = per.extraerTratamiento("");
		if (al.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay tratamientos disponibles.");
			return;
		}
		
		TratamientosPorAnimalGUI tranim = new TratamientosPorAnimalGUI(this);
		setEnabled(false);
		tranim.setVisible(true);
	}

	protected void abrirConsAlim() {
		 List<Animal> al2 = per.extraerAnimal("","");
			if (al2.isEmpty()) {
				JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay animales disponibles.");
				return;
			}
		
		ConsumoAlimentosGUI cons = new ConsumoAlimentosGUI(this);
		setEnabled(false);
		cons.setVisible(true);
	}

	protected void abrirCapacitEmpleados() {
		List<Empleado> al2 = per.extraerEmpleado("", "");
		if (al2.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay empleados disponibles.");
			return;
		}
		List<Tratamiento> al = per.extraerTratamiento("");
		if (al.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay tratamientos disponibles.");
			return;
		}
		
		EmpleadosTratamientoGUI empt = new EmpleadosTratamientoGUI(this);
		setEnabled(false);
		empt.setVisible(true);
	}

	protected void abrirNominas() {
		 List<Empleado> al2 = per.extraerEmpleado("", "");
			if (al2.isEmpty()) {
				JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay empleados disponibles.");
				return;
			}
		
		NominasGUI nom = new NominasGUI(this);
		setEnabled(false);
		nom.setVisible(true);
		
	}

	protected void abrirTrabPorZona() {
		 List<Zona> al2 = per.extraerZona("");
			if (al2.isEmpty()) {
				JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay zonas disponibles.");
				return;
			}
		
		TrabajadoresZonaGUI tra = new TrabajadoresZonaGUI(this);
		setEnabled(false);
		tra.setVisible(true);
	}

	protected void mostrarEmpleados() {
		EmpleadosGUI em = new EmpleadosGUI(this);
		setEnabled(false);
		em.setVisible(true);
	}

	protected void abrirEventos() {
		EventosGUI ev = new EventosGUI(this);
		setEnabled(false);
		ev.setVisible(true);
	}

	protected void abrirAnimales() {
		 List<Especie> al = per.extraerEspecie("");
		if (al.isEmpty()) {
			JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay especies disponibles.");
			return;
		}
		
		 List<Zona> al2 = per.extraerZona("");
			if (al2.isEmpty()) {
				JOptionPane.showMessageDialog(this, "No se ha podido abrir porque no hay zonas disponibles.");
				return;
			}
		
		AnimalesGUI a= new AnimalesGUI(this);
		setEnabled(false);
		a.setVisible(true);
	}

	protected void abrirTratamientos() {
		TratamientosGUI t = new TratamientosGUI(this);
		setEnabled(false);
		t.setVisible(true);
	}

	protected void abrirAlimentos() {
		AlimentosGUI a = new AlimentosGUI(this);
		setEnabled(false);
		a.setVisible(true);
	
	}

	protected void abrirEspecies() {
		EspecieGUI es = new EspecieGUI(this);
		setEnabled(false);
		es.setVisible(true);
	}

	protected void abrirZona() {
		ZonaGUI z = new ZonaGUI(this);
		setEnabled(false);
		z.setVisible(true);
		
	}
}
