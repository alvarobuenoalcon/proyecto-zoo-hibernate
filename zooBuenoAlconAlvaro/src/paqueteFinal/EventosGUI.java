package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Evento;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EventosGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Evento> alEventos;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldEvento;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JTextField textFieldCoste;
	private JTextField textFieldEmpieza;
	private JLabel lblEmpieza_1;
	private JTextField textFieldAcaba;
	private JLabel lblAcaba_1;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EventosGUI frame = new EventosGUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public EventosGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de EVENTOS");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 650, 497);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(219, 112, 147));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(118, 11, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(35, 14, 21, 14);
		contentPane.add(lblId);

		textFieldEvento = new JTextField();
		textFieldEvento.setColumns(10);
		textFieldEvento.setBounds(118, 42, 86, 20);
		contentPane.add(textFieldEvento);

		JLabel lblZona = new JLabel("EVENTO");
		lblZona.setBounds(35, 45, 73, 14);
		contentPane.add(lblZona);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(231, 11, 89, 53);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(231, 88, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(231, 134, 89, 23);
		contentPane.add(btnBorrar);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 260, 589, 187);
		contentPane.add(scrollPane);
		
				table = new JTable();
				table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {

						btnGuardar.setEnabled(true);
						btnBorrar.setEnabled(true);

						try {
							actualizarFormulario();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					
					}
				});
				scrollPane.setViewportView(table);
				
						ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
				////
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				table.setDefaultEditor(Object.class, null);

		JButton btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(342, 11, 164, 87);
		contentPane.add(btnLimpiar);

		textFieldCoste = new JTextField();
		textFieldCoste.setColumns(10);
		textFieldCoste.setBounds(118, 73, 86, 20);
		contentPane.add(textFieldCoste);

		JLabel lblCoste = new JLabel("COSTE");
		lblCoste.setBounds(35, 76, 73, 14);
		contentPane.add(lblCoste);
		
		textFieldEmpieza = new JTextField();
		textFieldEmpieza.setToolTipText("HH:MM");
		textFieldEmpieza.setColumns(10);
		textFieldEmpieza.setBounds(118, 104, 86, 20);
		contentPane.add(textFieldEmpieza);
		
		lblEmpieza_1 = new JLabel("EMPIEZA");
		lblEmpieza_1.setBounds(35, 107, 73, 14);
		contentPane.add(lblEmpieza_1);
		
		textFieldAcaba = new JTextField();
		textFieldAcaba.setToolTipText("HH:MM");
		textFieldAcaba.setColumns(10);
		textFieldAcaba.setBounds(118, 135, 86, 20);
		contentPane.add(textFieldAcaba);
		
		lblAcaba_1 = new JLabel("ACABA");
		lblAcaba_1.setBounds(35, 138, 73, 14);
		contentPane.add(lblAcaba_1);
		model.addColumn("ID");
		model.addColumn("Evento");
		model.addColumn("Precio");
		model.addColumn("Inicio");
		model.addColumn("Fin");
		table.setFillsViewportHeight(true);
	}

	protected void borrar() {

		if (textFieldID.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No hay ningun evento seleccionado para borrar.");
			return;
		}

		if (JOptionPane.showConfirmDialog(this, "Desea borrar el evento con id: " + textFieldID.getText() + "?",
				"CUIDADO", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			String cadenaEspecie = textFieldEvento.getText().toUpperCase();

			Evento e = new Evento();
			e.setId(Integer.valueOf(textFieldID.getText()));
			Evento e1 = AppMain.per.buscarEvento(e);
			System.out.println("borrar a " + e1.getDescripcion());
			
			//Si el alimento tiene animales asociados no se puede borrar
			if (!AppMain.per.buscarEvento(e1).getEntradas().isEmpty()) {
				JOptionPane.showMessageDialog(this, "No puedes borrar el evento porque tiene entradas asociados.");
				return;
			}
			
			AppMain.per.borrarEvento(e1);
			DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
			model.setRowCount(0);
			btnGuardar.setEnabled(false);
			btnBorrar.setEnabled(false);
			textFieldID.setText("");
			textFieldEvento.setText("");
			textFieldCoste.setText("");
			textFieldEmpieza.setText("");
			textFieldAcaba.setText("");
			JOptionPane.showMessageDialog(this, "El evento se ha borrado correctamente.");
		}

	}

	protected void guardar() {
		if (textFieldEvento.getText().length()==0) {
			JOptionPane.showMessageDialog(this, "El campo de evento no puede estar vacío");
			return;
		}
		Double coste = 0.0;
		if (textFieldCoste.getText().equals("")) {
			textFieldCoste.setText("0.0");
		}
		try { // COMPRUEBO QUE SEA UN DOUBLE EL COSTE
			coste = Double.valueOf(textFieldCoste.getText());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "El formato de coste debe ser: NUM.DEC");
			e.printStackTrace();
			return;

		}
		if (Double.valueOf(textFieldCoste.getText())<0) {
			JOptionPane.showMessageDialog(this, "El coste no puede ser negativo");
			return;
		}
		
		

		if (!textFieldID.getText().equals("")) {// compruebo que sea un id ya existente para update

			if (JOptionPane.showConfirmDialog(this,"Desea sobreescribir el evento con id: " + textFieldID.getText() + "?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Evento alExiste=AppMain.per.buscarEvento(new Evento(textFieldEvento.getText().trim().toUpperCase(), Double.valueOf(textFieldCoste.getText())));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null && alExiste.getId()!=Integer.parseInt(textFieldID.getText())) {
					JOptionPane.showMessageDialog(this, "No se pudo sobreescribir el evento porque ya hay uno con el mismo nombre.");
					return;
				}
				
				Evento alPrueba = new Evento();
				alPrueba.setId(Integer.valueOf(textFieldID.getText()));
				Evento al = AppMain.per.buscarEvento(alPrueba);
				al.setDescripcion(textFieldEvento.getText().trim().toUpperCase());
				al.setPrecio(coste);
				
				if (textFieldEmpieza.getText().length()>0) {
					System.out.println(textFieldEmpieza.getText()+" "+ textFieldEmpieza.getText().length());
					DateFormat dateFormat = new SimpleDateFormat("HH:mm"); 
					 try {
						 al.setHoraInicio(dateFormat.parse(textFieldEmpieza.getText().trim()));
					} catch (ParseException e) {
						JOptionPane.showMessageDialog(this, "El formato de Empieza debe ser: HH:mm");
						e.printStackTrace();
						return;
					}
				}else {
					al.setHoraInicio(null);
				}
				
				if (textFieldAcaba.getText().length()>0) {
					System.out.println(textFieldAcaba.getText()+" "+ textFieldAcaba.getText().length());
					DateFormat dateFormat = new SimpleDateFormat("HH:mm"); 
					 try {
						 al.setHoraFin(dateFormat.parse(textFieldAcaba.getText().trim()));
					} catch (ParseException e) {
						JOptionPane.showMessageDialog(this, "El formato de Acaba debe ser: HH:mm");
						e.printStackTrace();
						return;
					}
				}else {
					al.setHoraFin(null);
				}
				
				
				
				try {
					AppMain.per.guardarEvento(al);
					JOptionPane.showMessageDialog(this, "Evento guardado correctamente.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} else {// creo uno nuevo
			if (JOptionPane.showConfirmDialog(this,"Desea crear el evento  " + textFieldEvento.getText().trim().toUpperCase() + "?", "CONFIRMAR",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Evento alExiste=AppMain.per.buscarEvento(new Evento(textFieldEvento.getText().trim().toUpperCase(), Double.valueOf(textFieldCoste.getText())));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null) {
					JOptionPane.showMessageDialog(this, "No se pudo guardar el evento porque ya hay uno con el mismo nombre.");
					return;
				}
				Evento alPrueba = new Evento();
			alPrueba.setPrecio(coste);
			alPrueba.setDescripcion(textFieldEvento.getText().trim().toUpperCase());
			//le pongo hora inicio
			if (textFieldEmpieza.getText().length()>0) {
				System.out.println(textFieldEmpieza.getText()+" "+ textFieldEmpieza.getText().length());
				DateFormat dateFormat = new SimpleDateFormat("HH:mm"); 
				 try {
					 alPrueba.setHoraInicio(dateFormat.parse(textFieldEmpieza.getText().trim()));
				} catch (ParseException e) {
					JOptionPane.showMessageDialog(this, "El formato de Empieza debe ser: HH:mm");
					e.printStackTrace();
					return;
				}
			}else {
				alPrueba.setHoraInicio(null);
			}
			
			//le pongo hora final
			if (textFieldAcaba.getText().length()>0) {
				System.out.println(textFieldAcaba.getText()+" "+ textFieldAcaba.getText().length());
				DateFormat dateFormat = new SimpleDateFormat("HH:mm"); 
				 try {
					 alPrueba.setHoraFin(dateFormat.parse(textFieldAcaba.getText().trim()));
				} catch (ParseException e) {
					JOptionPane.showMessageDialog(this, "El formato de Acaba debe ser: HH:mm");
					e.printStackTrace();
					return;
				}
			}else {
				alPrueba.setHoraFin(null);
			}
			//lo creo
			try {
				AppMain.per.guardarEvento(alPrueba);
				JOptionPane.showMessageDialog(this, "evento guardado correctamente.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			
		}
		inicio();
		
	}

	

	protected void buscar() {
		textFieldCoste.setText("");
		textFieldID.setText("");
		textFieldEmpieza.setText("");
		textFieldAcaba.setText("");
		textFieldEvento.setText(textFieldEvento.getText().trim().toUpperCase());
	

			aniadirTabla();
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);


	}

	private void aniadirTabla() {
		alEventos = AppMain.per.extraerEvento(textFieldEvento.getText().toUpperCase().trim());
		System.out.println(alEventos.size());
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alEventos.size(); i++) {
			Object[] f = new Object[5];
			f[0] = alEventos.get(i).getId();// añado id
			f[1] = alEventos.get(i).getDescripcion();// añado descripcion
			f[2] = alEventos.get(i).getPrecio();
			
			Date date = alEventos.get(i).getHoraInicio();
			String strDate="";
			System.out.println();
			if (date!=null) {
				DateFormat dateFormat = new SimpleDateFormat("HH:mm");  
				 strDate = dateFormat.format(date);
				 System.out.println("date date : "+strDate);
			}
			System.out.println("date date : "+strDate);
			f[3] = strDate;
			
			
			 date = alEventos.get(i).getHoraFin();
			 strDate="";
			System.out.println();
			if (date!=null) {
				DateFormat dateFormat = new SimpleDateFormat("HH:mm");  
				 strDate = dateFormat.format(date);
				 System.out.println("date date : "+strDate);
			}
			System.out.println("date date : "+strDate);
			f[4] = strDate;

			model.addRow(f);// lo inserto en tabla
		}

	}

	protected void actualizarFormulario() {
		if (table.getSelectedRow() != -1) {
			System.out.println("size array:" + alEventos.size());
			System.out.println("pos tabla " + table.getSelectedRow());
			textFieldEvento.setText(alEventos.get(table.getSelectedRow()).getDescripcion());
			textFieldID.setText(String.valueOf(alEventos.get(table.getSelectedRow()).getId()));
			textFieldCoste.setText(String.valueOf(alEventos.get(table.getSelectedRow()).getPrecio()));
			
			Date dateI = alEventos.get(table.getSelectedRow()).getHoraInicio(); 
			String inicio="";
			if (dateI!=null) {
				DateFormat dateFormat = new SimpleDateFormat("HH:mm");  
				 inicio = dateFormat.format(dateI);
			}
			textFieldEmpieza.setText(inicio);
			
			Date dateF = alEventos.get(table.getSelectedRow()).getHoraFin(); 
			 String acaba="";
			if (dateF!=null) {
				DateFormat dateFormat = new SimpleDateFormat("HH:mm");  
				acaba = dateFormat.format(dateF);
			}
			textFieldAcaba.setText(acaba);
			
			
			System.out.println("puesto");
		}

	}

	private void inicio() {

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldEvento.setText("");
		textFieldCoste.setText("");
		textFieldEmpieza.setText("");
		textFieldAcaba.setText("");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
	}
}
