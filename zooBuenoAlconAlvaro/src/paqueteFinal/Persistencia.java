package paqueteFinal;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import hibernate.Alimento;
import hibernate.Animal;
import hibernate.Animaltratamiento;
import hibernate.Empleado;
import hibernate.Entrada;
import hibernate.Especie;
import hibernate.Evento;
import hibernate.Nomina;
import hibernate.Tratamiento;
import hibernate.Zona;

public interface Persistencia {
	//general
	void guardarSinCommit(Object o);
	void borrarSinCommit(Object o);
	void transaccionCommit();
	void transaccionRollback();
	void transaccionRefresh(Object o);
	
	//Zona
	Zona buscarZona(Zona z1);
	int guardarZona(Zona z) throws SQLException, ParseException;
	List<Zona> extraerZona(String descripcion);
	public void borrarZona(Zona z);
	
	//Especie
	Especie buscarEspecie(Especie e1);
	int guardarEspecie(Especie e) throws SQLException, ParseException;
	List<Especie> extraerEspecie(String descripcion);
	void borrarEspecie(Especie e);
	
	//Alimentos
	Alimento buscarAlimento(Alimento a1);
	int guardarAlimento(Alimento a) throws SQLException, ParseException;
	void borrarAlimento(Alimento a);
	List<Alimento> extraerAlimento(String text);
	
	//Tratamientos
	Tratamiento buscarTratamiento(Tratamiento t1);
	int guardarTratamiento(Tratamiento t) throws SQLException, ParseException;
	void borrarTratamiento(Tratamiento t);
	List<Tratamiento> extraerTratamiento(String text);
	
	//Animales
	Animal buscarAnimal(Animal a);
	int guardarAnimal(Animal e) throws SQLException, ParseException;
	void borrarAnimal(Animal e1);
	List<Animal> extraerAnimal(String nombre, String especie);
	
	//empleados
	public Empleado buscarEmpleado(Empleado e);
	int guardarEmpleado(Empleado e) throws SQLException, ParseException;
	void borrarEmpleado(Empleado e1);
	List<Empleado> extraerEmpleado(String nombre, String direccion);
	
	// eventos
	public Evento buscarEvento(Evento e);
	int guardarEvento(Evento e) throws SQLException, ParseException;
	void borrarEvento(Evento e1);
	List<Evento> extraerEvento(String nombre);
	//proceso entradas
	public Entrada buscarEntrada(Entrada e);
	void borrarEntrada(Entrada e1);
	List<Entrada> extraerEntrada(String nombre);
	//proceso nominas
	Nomina buscarNomina(Nomina e);
	void borrarNomina(Nomina e1);
	List<Nomina> extraerNomina(String id);
	int guardarNomina(Nomina e) throws SQLException, ParseException;
	//extraer tratporespecie
	List<Animaltratamiento> extraerTratPorEspecie(Date fechaDesde, Date fechaHasta, Especie e);
}
