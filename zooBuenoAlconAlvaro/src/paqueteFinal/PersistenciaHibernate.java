package paqueteFinal;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import hibernate.Alimento;
import hibernate.Animal;
import hibernate.Animaltratamiento;
import hibernate.Empleado;
import hibernate.Entrada;
import hibernate.Especie;
import hibernate.Evento;
import hibernate.Nomina;
import hibernate.Tratamiento;
import hibernate.Zona;

public class PersistenciaHibernate implements Persistencia {

	Session sesion;

	public PersistenciaHibernate(String fichCfg, boolean mostrarSQL) {
		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure(fichCfg);
		if (mostrarSQL)
			configuration.setProperty("hibernate.show_sql", "true");
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		sesion = sessionFactory.openSession();

	}

	// ZONAS

	public Zona buscarZona(Zona z1) {
		Zona z2 = new Zona();
		if (z1.getId() != null) {
			System.out.println("busqueda por id");
			Query q = sesion.createQuery("SELECT z FROM Zona z WHERE id=?");
			q.setString(0, String.valueOf(z1.getId()));
			z2 = (Zona) q.uniqueResult();
		} else {
			Query q = sesion.createQuery("SELECT z FROM Zona z WHERE descripcion=?");
			q.setString(0, z1.getDescripcion());
			z2 = (Zona) q.uniqueResult();

		}

		return z2;

	}

	public int guardarZona(Zona z) throws SQLException, ParseException {
		sesion.beginTransaction();
		sesion.save(z);
		sesion.getTransaction().commit();
		return z.getId();

	}

	public List<Zona> extraerZona(String descripcion) {
		String sentencia = "SELECT z FROM Zona z WHERE descripcion LIKE '%" + descripcion + "%'";
		Query query = sesion.createQuery(sentencia);
		List<Zona> Zona = query.list();
		return Zona;
	}

	public void borrarZona(Zona z) {
		
			sesion.beginTransaction();// Borramos la zona
			sesion.delete(z);
			sesion.getTransaction().commit();

		
	}

	// ESPECIES

	public Especie buscarEspecie(Especie e1) {
		Especie e2 = new Especie();
		if (e1.getId() != null) {
			Query q = sesion.createQuery("SELECT e FROM Especie e WHERE id=?");
			q.setString(0, String.valueOf(e1.getId()));
			e2 = (Especie) q.uniqueResult();
		} else {
			Query q = sesion.createQuery("SELECT e FROM Especie e WHERE descripcion=?");
			q.setString(0, e1.getDescripcion());
			e2 = (Especie) q.uniqueResult();

		}

		return e2;

	}

	public int guardarEspecie(Especie e) throws SQLException, ParseException {
		sesion.beginTransaction();
		sesion.save(e);
		sesion.getTransaction().commit();
		return e.getId();

	}

	public List<Especie> extraerEspecie(String descripcion) {
		String sentencia = "SELECT e FROM Especie e WHERE descripcion LIKE '%" + descripcion + "%'";
		Query query = sesion.createQuery(sentencia);
		List<Especie> Especie = query.list();
		return Especie;
	}

	public void borrarEspecie(Especie e) {
		sesion.beginTransaction();// Borramos la especie

		sesion.delete(e);
		sesion.getTransaction().commit();
	}

	// Alimentos

	public Alimento buscarAlimento(Alimento a1) {
		System.out.println(a1.getId());
		Alimento a2 = new Alimento();
		if (a1.getId() != null) {
			Query q = sesion.createQuery("SELECT a FROM Alimento a WHERE id=?");
			q.setString(0, String.valueOf(a1.getId()));
			a2 = (Alimento) q.uniqueResult();
		} else {
			Query q = sesion.createQuery("SELECT a FROM Alimento a WHERE descripcion=?");
			q.setString(0, a1.getDescripcion());
			a2 = (Alimento) q.uniqueResult();

		}

		return a2;

	}

	public int guardarAlimento(Alimento a) throws SQLException, ParseException {
		sesion.beginTransaction();
		sesion.save(a);
		sesion.getTransaction().commit();
		return a.getId();

	}

	public void borrarAlimento(Alimento a) {
		sesion.beginTransaction();// Borramos el alimento

		sesion.delete(a);
		sesion.getTransaction().commit();
	}

	@Override
	public List<Alimento> extraerAlimento(String text) {
		String sentencia = "SELECT e FROM Alimento e WHERE descripcion LIKE '%" + text + "%'";
		Query query = sesion.createQuery(sentencia);
		List<Alimento> Alimento = query.list();
		return Alimento;
	}

	// Tratamientos

	public Tratamiento buscarTratamiento(Tratamiento a1) {
		Tratamiento a2 = new Tratamiento();
		if (a1.getId() != null) {
			Query q = sesion.createQuery("SELECT a FROM Tratamiento a WHERE id=?");
			q.setString(0, String.valueOf(a1.getId()));
			a2 = (Tratamiento) q.uniqueResult();
		} else {
			Query q = sesion.createQuery("SELECT a FROM Tratamiento a WHERE descripcion=?");
			q.setString(0, a1.getDescripcion());
			a2 = (Tratamiento) q.uniqueResult();

		}

		return a2;

	}

	public int guardarTratamiento(Tratamiento a) throws SQLException, ParseException {
		sesion.beginTransaction();
		sesion.save(a);
		sesion.getTransaction().commit();
		return a.getId();

	}

	public void borrarTratamiento(Tratamiento a) {
		sesion.beginTransaction();// Borramos el tratamiento

		sesion.delete(a);
		sesion.getTransaction().commit();
	}

	@Override
	public List<Tratamiento> extraerTratamiento(String text) {
		String sentencia = "SELECT e FROM Tratamiento e WHERE descripcion LIKE '%" + text + "%'";
		Query query = sesion.createQuery(sentencia);
		List<Tratamiento> Tratamiento = query.list();
		return Tratamiento;
	}

	// Animales
	@Override
	public Animal buscarAnimal(Animal a) {
		Animal a2 = new Animal();
		if (a.getId() != null) {
			Query q = sesion.createQuery("SELECT a FROM Animal a WHERE id=?");
			q.setString(0, String.valueOf(a.getId()));
			a2 = (Animal) q.uniqueResult();
		} else {
			Query q = sesion.createQuery("SELECT a FROM Animal a WHERE nombre=?");
			q.setString(0, a.getNombre());
			a2 = (Animal) q.uniqueResult();

		}

		return a2;

	}

	@Override
	public void borrarAnimal(Animal e1) {

		sesion.beginTransaction();// Borramos el alimento

		sesion.delete(e1);
		sesion.getTransaction().commit();

	}

	@Override
	public List<Animal> extraerAnimal(String nombre, String especie) {
		if (especie.equals("")) {
			Especie e = buscarEspecie(new Especie(especie));
			String sentencia = "SELECT e FROM Animal e WHERE nombre LIKE '%" + nombre + "%' AND especie LIKE '%" + especie+ "%'";
			Query query = sesion.createQuery(sentencia);
			List<Animal> Animal = query.list();
			return Animal;
		}
		
		Especie e = buscarEspecie(new Especie(especie));
		String sentencia = "SELECT e FROM Animal e WHERE nombre LIKE '%" + nombre + "%' AND especie LIKE '%" + e.getId()+ "%'";
		Query query = sesion.createQuery(sentencia);
		List<Animal> Animal = query.list();
		return Animal;
	}

	@Override
	public int guardarAnimal(Animal e) throws SQLException, ParseException {
		sesion.beginTransaction();
		sesion.save(e);
		sesion.getTransaction().commit();
		return e.getId();
	}

	// Empleados

	@Override
	public Empleado buscarEmpleado(Empleado e) {

		Empleado a2 = new Empleado();
		if (e.getId() != null) {
			Query q = sesion.createQuery("SELECT a FROM Empleado a WHERE id=?");
			q.setString(0, String.valueOf(e.getId()));
			a2 = (Empleado) q.uniqueResult();
		} else {
			Query q = sesion.createQuery("SELECT a FROM Empleado a WHERE nombre=?");
			q.setString(0, e.getNombre());
			a2 = (Empleado) q.uniqueResult();

		}

		return a2;

	}

	@Override
	public int guardarEmpleado(Empleado e) throws SQLException, ParseException {

		sesion.beginTransaction();
		sesion.save(e);
		sesion.getTransaction().commit();
		return e.getId();
	}

	@Override
	public void borrarEmpleado(Empleado e1) {
		sesion.beginTransaction();// Borramos el tratamiento
		sesion.delete(e1);
		sesion.getTransaction().commit();

	}

	@Override
	public List<Empleado> extraerEmpleado(String nombre, String direccion) {
		String sentencia = "SELECT e FROM Empleado e WHERE nombre LIKE '%" + nombre + "%' AND direccion LIKE '%"
				+ direccion + "%'";
		Query query = sesion.createQuery(sentencia);
		List<Empleado> Empleado = query.list();
		return Empleado;
	}

	// Eventos
	@Override
	public Evento buscarEvento(Evento e) {
		Evento a2 = new Evento();
		if (e.getId() != null) {
			Query q = sesion.createQuery("SELECT a FROM Evento a WHERE id=?");
			q.setString(0, String.valueOf(e.getId()));
			a2 = (Evento) q.uniqueResult();
		} else {
			Query q = sesion.createQuery("SELECT a FROM Evento a WHERE descripcion=?");
			q.setString(0, e.getDescripcion());
			a2 = (Evento) q.uniqueResult();

		}

		return a2;

	}

	@Override
	public int guardarEvento(Evento e) throws SQLException, ParseException {
		sesion.beginTransaction();
		sesion.save(e);
		sesion.getTransaction().commit();
		return e.getId();
	}

	@Override
	public void borrarEvento(Evento e1) {
		sesion.beginTransaction();// Borramos el tratamiento

		sesion.delete(e1);
		sesion.getTransaction().commit();

	}

	@Override
	public List<Evento> extraerEvento(String descripcion) {
		String sentencia = "SELECT e FROM Evento e WHERE descripcion LIKE '%" + descripcion + "%'";
		Query query = sesion.createQuery(sentencia);
		List<Evento> Evento = query.list();
		return Evento;
	}

	// Proceso entradas

	@Override
	public Entrada buscarEntrada(Entrada e) {

		Entrada a2 = new Entrada();
		if (e.getId() != null) {
			Query q = sesion.createQuery("SELECT a FROM Entrada a WHERE id=?");
			q.setString(0, String.valueOf(e.getId()));
			a2 = (Entrada) q.uniqueResult();
		}

		return a2;

	}

	@Override
	public void borrarEntrada(Entrada e1) {
		sesion.beginTransaction();// Borramos el tratamiento

		sesion.delete(e1);
		sesion.getTransaction().commit();

	}

	@Override
	public List<Entrada> extraerEntrada(String id) {
		String sentencia = "SELECT e FROM Entrada e WHERE idevento LIKE '%" + id + "%' ORDER BY  fechaHoraVenta DESC";
		Query query = sesion.createQuery(sentencia);
		List<Entrada> Entrada = query.list();
		return Entrada;
	}
	// Proceso nominas

	@Override
	public int guardarNomina(Nomina e) throws SQLException, ParseException {
		sesion.beginTransaction();
		sesion.save(e);
		sesion.getTransaction().commit();
		return e.getId();
	}
	
		@Override
		public Nomina buscarNomina(Nomina e) {

			Nomina a2 = new Nomina();
			
				Query q = sesion.createQuery("SELECT a FROM Nomina a WHERE id=?");
				q.setString(0, String.valueOf(e.getId()));
				a2 = (Nomina) q.uniqueResult();
			

			return a2;

		}

		@Override
		public void borrarNomina(Nomina e1) {
			sesion.beginTransaction();// Borramos el tratamiento

			sesion.delete(e1);
			sesion.getTransaction().commit();

		}

		@Override
		public List<Nomina> extraerNomina(String id) {
			String sentencia = "SELECT e FROM Nomina e WHERE idempleado LIKE '%" + id + "%' ORDER BY  fechaEmision DESC";
			Query query = sesion.createQuery(sentencia);
			List<Nomina> Nomina = query.list();
			return Nomina;
		}
		
		@Override
		public List<Animaltratamiento> extraerTratPorEspecie(Date desde ,Date hasta, Especie e) {
			String fechaDesde="";
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			fechaDesde = dateFormat.format(desde);
			
			String fechaHasta="";
			fechaHasta = dateFormat.format(hasta);
			
			List<Animal> alAnimales = extraerAnimal("", e.getDescripcion());
			String animales="(";
			
			for (int i = 0; i < alAnimales.size(); i++) {
				if (i==alAnimales.size()-1) {
					animales+=String.valueOf(alAnimales.get(i).getId())+")";
				}else {
					animales+=String.valueOf(alAnimales.get(i).getId())+",";
				}
			}
			
			
			
			String sentencia = "SELECT e FROM Animaltratamiento e WHERE (fechaHora BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"') AND idAnimal IN "+animales+" ORDER BY  fechaHora DESC";
			Query query = sesion.createQuery(sentencia);
			List<Animaltratamiento> Animaltratamiento = query.list();
			return Animaltratamiento;
		}
	
	
	
	//commit no commit
	public void guardarSinCommit(Object o) {
		if (sesion.getTransaction() != null && sesion.getTransaction().isActive()) {
			sesion.save(o);

		} else {
			sesion.beginTransaction();
			sesion.save(o);
		}

	}

	public void borrarSinCommit(Object o) {
		if (sesion.getTransaction() != null && sesion.getTransaction().isActive()) {
			sesion.delete(o);
		} else {
			sesion.beginTransaction();
			sesion.delete(o);
		}

	}

	public void transaccionCommit() {
		if (sesion.getTransaction() != null && sesion.getTransaction().isActive()) {
			sesion.getTransaction().commit();

		}
	}

	public void transaccionRollback() {
		if (sesion.getTransaction() != null && sesion.getTransaction().isActive()) {
			sesion.getTransaction().rollback();

		}
	}

	public void transaccionRefresh(Object o) {
		if (sesion.getTransaction() != null && sesion.getTransaction().isActive()) {

			sesion.refresh(o);
		}
	}

}
