package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Alimento;
import hibernate.Animal;
import hibernate.Consume;
import hibernate.ConsumeId;
import hibernate.Especie;
import hibernate.Zona;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class ConsumoAlimentosGUI extends JFrame {
	static javax.swing.JFrame padre;
	Animal animal;
	boolean animalSeleccionado;
	Alimento alimento;
	boolean alimentoSeleccionado;

	List<Animal> alAnimales;
	List<Especie> lEspecie;
	List<Alimento> lAlimento;
	ArrayList<Consume> lConsume;
	ArrayList<Consume> lConsumeBorrar;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldNombre;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscarAnimal;
	private JTable tableAnimal;
	private JComboBox comboBoxEspecie;
	private JScrollPane scrollPane;
	private JTable tableConsumos;
	private JTextField textFieldAlimentoGuardar;
	private JTextField textFieldCantidad;
	private JLabel lblAlimentoInv;
	private JLabel lblCantidadInv;
	private JComboBox comboBoxAlimento;
	private JButton btnSeleccionarAlimento;
	private JButton buttonBuscarConsumos;
	private JButton btnSeleccionarAnimal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsumoAlimentosGUI frame = new ConsumoAlimentosGUI(padre);

					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public ConsumoAlimentosGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Consumo de alimentos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1016, 649);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(83, 31, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(10, 34, 21, 14);
		contentPane.add(lblId);

		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(83, 62, 86, 20);
		contentPane.add(textFieldNombre);

		JLabel lblZona = new JLabel("NOMBRE");
		lblZona.setBounds(10, 65, 54, 14);
		contentPane.add(lblZona);

		btnBuscarAnimal = new JButton("Buscar animal");
		btnBuscarAnimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscarAnimal.setBounds(10, 121, 159, 32);
		contentPane.add(btnBuscarAnimal);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setBounds(879, 566, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(10, 417, 159, 23);
		contentPane.add(btnBorrar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(189, 33, 779, 163);
		contentPane.add(scrollPane);

		tableAnimal = new JTable();
		tableAnimal.addMouseListener(new MouseAdapter() {

		});
		scrollPane.setViewportView(tableAnimal);

		ListSelectionModel listSelectionModel = tableAnimal.getSelectionModel();
		tableAnimal.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		tableAnimal.setDefaultEditor(Object.class, null);

		JButton btnLimpiar = new JButton("INICIO");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(189, 554, 140, 46);
		contentPane.add(btnLimpiar);

		JLabel lblCoste = new JLabel("ESPECIE");
		lblCoste.setBounds(10, 96, 73, 14);
		contentPane.add(lblCoste);

		comboBoxEspecie = new JComboBox();
		comboBoxEspecie.setBounds(83, 93, 86, 20);
		contentPane.add(comboBoxEspecie);

		JLabel lblAnimal = new JLabel("ANIMAL");
		lblAnimal.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAnimal.setBounds(523, 8, 126, 14);
		contentPane.add(lblAnimal);

		btnSeleccionarAnimal = new JButton("Seleccionar animal");
		btnSeleccionarAnimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarAnimal();
			}
		});
		btnSeleccionarAnimal.setBounds(10, 164, 159, 32);
		contentPane.add(btnSeleccionarAnimal);

		btnSeleccionarAlimento = new JButton("Seleccionar alimento");
		btnSeleccionarAlimento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarAlimento();
			}
		});
		btnSeleccionarAlimento.setBounds(405, 252, 159, 32);
		contentPane.add(btnSeleccionarAlimento);

		buttonBuscarConsumos = new JButton("Buscar consumos");
		buttonBuscarConsumos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarConsumos();
			}
		});
		buttonBuscarConsumos.setFont(new Font("Tahoma", Font.BOLD, 12));
		buttonBuscarConsumos.setBounds(10, 344, 159, 51);
		contentPane.add(buttonBuscarConsumos);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(189, 344, 779, 163);
		contentPane.add(scrollPane_2);

		tableConsumos = new JTable();
		tableConsumos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				btnBorrar.setEnabled(true);
			}
		});
		tableConsumos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableConsumos.setFillsViewportHeight(true);
		scrollPane_2.setViewportView(tableConsumos);

		textFieldAlimentoGuardar = new JTextField();
		textFieldAlimentoGuardar.setEnabled(false);
		textFieldAlimentoGuardar.setText("");
		textFieldAlimentoGuardar.setColumns(10);
		textFieldAlimentoGuardar.setBounds(659, 567, 86, 20);
		contentPane.add(textFieldAlimentoGuardar);

		textFieldCantidad = new JTextField();
		textFieldCantidad.setEnabled(false);
		textFieldCantidad.setText("");
		textFieldCantidad.setColumns(10);
		textFieldCantidad.setBounds(769, 567, 86, 20);
		contentPane.add(textFieldCantidad);

		lblAlimentoInv = new JLabel("ALIMENTO");
		lblAlimentoInv.setBounds(659, 543, 54, 14);
		contentPane.add(lblAlimentoInv);

		lblCantidadInv = new JLabel("CANTIDAD");
		lblCantidadInv.setBounds(769, 543, 54, 14);
		contentPane.add(lblCantidadInv);

		comboBoxAlimento = new JComboBox();
		comboBoxAlimento.setBounds(574, 252, 190, 32);
		contentPane.add(comboBoxAlimento);

		JLabel lblAlimento = new JLabel("ALIMENTO");
		lblAlimento.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAlimento.setBounds(522, 227, 126, 14);
		contentPane.add(lblAlimento);

		JLabel lblConsumos = new JLabel("CONSUMOS");
		lblConsumos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblConsumos.setBounds(523, 319, 126, 14);
		contentPane.add(lblConsumos);

		////
		DefaultTableModel model = (DefaultTableModel) tableAnimal.getModel();

		model.addColumn("ID");
		model.addColumn("Nombre");
		model.addColumn("Especie");
		model.addColumn("Zona");
		model.addColumn("FechaNac");
		tableAnimal.setFillsViewportHeight(true);

		DefaultTableModel model2 = (DefaultTableModel) tableConsumos.getModel();
		model2.addColumn("Animal");
		model2.addColumn("Alimento");
		model2.addColumn("Cantidad");
		tableConsumos.setFillsViewportHeight(true);

		JButton buttonConfirmar = new JButton("CONFIRMAR");
		buttonConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				confirmar();
			}
		});
		buttonConfirmar.setBounds(10, 554, 140, 46);
		contentPane.add(buttonConfirmar);

		inicio();
	}

	protected void confirmar() {
		if (JOptionPane.showConfirmDialog(this, "�Estas seguro de confirmar?", "CUIDADO",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
			for (Consume cons : lConsumeBorrar) {
				Animal a = new Animal();
				a.setId(cons.getAnimal().getId());
				a= AppMain.per.buscarAnimal(a);
				
				if (a.getConsumes().contains(cons)) {
					a.getConsumes().remove(cons);
				}
				//
				Alimento al = new Alimento();
				al.setId(cons.getAlimento().getId());
				al= AppMain.per.buscarAlimento(al);
				
				if (al.getConsumes().contains(cons)) {
					al.getConsumes().remove(cons);
				}
				//
				
				try {
					AppMain.per.guardarAnimal(a);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
			for (Consume cons : lConsume) {
				Animal a = new Animal();
				a.setId(cons.getAnimal().getId());
				a= AppMain.per.buscarAnimal(a);
				
				
				
				if (!a.getConsumes().contains(cons)) {
					a.getConsumes().add(cons);
				}
				
				Alimento al = new Alimento();
				al.setId(cons.getAlimento().getId());
				al= AppMain.per.buscarAlimento(al);
				
				if (!al.getConsumes().contains(cons)) {
					al.getConsumes().add(cons);
				}
				
				try {
					AppMain.per.guardarAnimal(a);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			lConsume.removeAll(lConsumeBorrar);
//			animal.getConsumes().clear();
//			animal.getConsumes().addAll(lConsume);
			
			
			
			JOptionPane.showMessageDialog(this, "Guardado correctamente.");
			inicio();
		}
		// TODO Auto-generated method stub

	}

	protected void buscarConsumos() {
		if (animalSeleccionado == false && alimentoSeleccionado == false) {
			JOptionPane.showMessageDialog(this, "Debes seleccionar al menos un animal o un alimento.");
			return;
		}
		
		// si he seleccionado los dos
		if (animalSeleccionado == true && alimentoSeleccionado == true) {
			lConsume = new ArrayList<>(animal.getConsumes());

			ArrayList lConsumeborrar = new ArrayList<>();
			for (int i = 0; i < lConsume.size(); i++) {
				if (!lConsume.get(i).getAlimento().getDescripcion().equals(alimento.getDescripcion())) {
					lConsumeborrar.add(lConsume.get(i));
				}
			}
			lConsume.removeAll(lConsumeborrar);
			
			if (lConsume.size() == 0) {
				crearNuevoConsumo();
			}

		}

		// si animal solo seleccionado
		if (animalSeleccionado == true && alimentoSeleccionado == false) {
			lConsume = new ArrayList<>(animal.getConsumes());
			if (lConsume.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay consumos disponibles.");
				return;
			}

		}

		// si extraerConsumosConAlimento select de consume
		if (animalSeleccionado == false && alimentoSeleccionado == true) {
			lConsume = new ArrayList<>(alimento.getConsumes());
			if (lConsume.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay consumos disponibles.");
				return;
			}

		}

		if (lConsume == null) {
			JOptionPane.showMessageDialog(this, "Debes seleccionar al menos animal o alimento.");
			return;
		}

		if (lConsume.size() > 0) {
			actualizarTablaConsume();
		}
	}

	private void actualizarTablaConsume() {

		DefaultTableModel model = (DefaultTableModel) tableConsumos.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < lConsume.size(); i++) {
			Object[] f = new Object[3];
			f[0] = lConsume.get(i).getAnimal().getNombre();// añado descripcion
			f[1] = lConsume.get(i).getAlimento().getDescripcion();
			f[2] = lConsume.get(i).getCantidadDia();

			model.addRow(f);// lo inserto en tabla
		}

	}

	private void crearNuevoConsumo() {
		if (JOptionPane.showConfirmDialog(this,
				"No existen consumos con esta busqueda. �Desea guardar un nuevo consumo?", "CUIDADO",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			textFieldAlimentoGuardar.setVisible(true);
			textFieldCantidad.setVisible(true);
			lblAlimentoInv.setVisible(true);
			lblCantidadInv.setVisible(true);
			btnGuardar.setVisible(true);
			textFieldCantidad.setEnabled(true);
			btnGuardar.setEnabled(true);
			textFieldAlimentoGuardar.setText(alimento.getDescripcion());
		}

	}

	protected void seleccionarAlimento() {
		if (lAlimento == null) {
			System.out.println("No hay alimentos disponibles");
			return;
		}

		alimento.setId(lAlimento.get(comboBoxAlimento.getSelectedIndex()).getId());
		alimento = AppMain.per.buscarAlimento(alimento);

		btnSeleccionarAlimento.setEnabled(false);
		comboBoxAlimento.setEnabled(false);
		alimentoSeleccionado = true;

		DefaultTableModel model2 = (DefaultTableModel) tableConsumos.getModel();// borro tabla
		model2.setRowCount(0);
	}

	protected void seleccionarAnimal() {
		if (tableAnimal.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(this, "No has seleccionado ningun animal");
			return;
		}

		// actualizo formulario
		textFieldNombre.setText(alAnimales.get(tableAnimal.getSelectedRow()).getNombre());
		textFieldID.setText(String.valueOf((alAnimales.get(tableAnimal.getSelectedRow()).getId())));
		// pongo a false lo del animal
		textFieldNombre.setEnabled(false);
		btnBuscarAnimal.setEnabled(false);
		btnSeleccionarAnimal.setEnabled(false);
		comboBoxEspecie.setEnabled(false);
		tableAnimal.setEnabled(false);
		// obtengo el animal
		animal.setId(Integer.valueOf(textFieldID.getText()));
		animal = AppMain.per.buscarAnimal(animal);
		animalSeleccionado = true;
		AppMain.per.transaccionRefresh(animal);
		
		DefaultTableModel model2 = (DefaultTableModel) tableConsumos.getModel();// borro tabla
		model2.setRowCount(0);

	}

	

	protected void borrar() {
		if (tableConsumos.getSelectedRow()==-1) {
			JOptionPane.showMessageDialog(this, "No ha seleccionado ningun consumo para borrar.");
			return;
		}
		
		Consume cons = lConsume.get(tableConsumos.getSelectedRow());
		lConsumeBorrar.add(cons);
		lConsume.remove(cons);
		// buscarConsumos();

		

		actualizarTablaConsume();
	}


	
	protected void guardar() {

		if (textFieldCantidad.getText().length()==0) {
			JOptionPane.showMessageDialog(this, "El campo de cantidad no puede estar vacio.");
			return;
		}

		
		int cantidad = 0;
		try {
			cantidad = Integer.parseInt(textFieldCantidad.getText());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "El campo de cantidad debe ser un entero.");
			e.printStackTrace();
			return;
		}
		
		if (cantidad<=0) {
			JOptionPane.showMessageDialog(this, "El campo de cantidad debe ser mayor que 0.");
			return;
		}

		ConsumeId consId = new ConsumeId();
		consId.setIdAlimento(alimento.getId());
		consId.setIdAnimal(animal.getId());

		Consume cons = new Consume(consId, animal, alimento, cantidad);
		lConsume.add(cons);

		
		actualizarTablaConsume();
		btnBorrar.setEnabled(false);

		textFieldAlimentoGuardar.setVisible(false);
		textFieldCantidad.setVisible(false);
		lblAlimentoInv.setVisible(false);
		lblCantidadInv.setVisible(false);
		btnGuardar.setVisible(false);

		textFieldAlimentoGuardar.setEnabled(false);
		textFieldCantidad.setEnabled(false);
		lblAlimentoInv.setEnabled(false);
		lblCantidadInv.setEnabled(false);
		btnGuardar.setEnabled(false);

	}

	protected void buscar() {
		aniadirTablaAnimal();

	}

	private void aniadirTablaAnimal() {
		alAnimales = AppMain.per.extraerAnimal(textFieldNombre.getText().toUpperCase().trim(),
				comboBoxEspecie.getSelectedItem().toString());

		System.out.println(alAnimales.size());
		DefaultTableModel model = (DefaultTableModel) tableAnimal.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alAnimales.size(); i++) {
			Object[] f = new Object[5];
			f[0] = alAnimales.get(i).getId();// añado id
			f[1] = alAnimales.get(i).getNombre();// añado descripcion
			f[2] = alAnimales.get(i).getEspecie().getDescripcion();
			f[3] = alAnimales.get(i).getZona().getDescripcion();

			Date date = alAnimales.get(i).getFechaNac();
			String strDate = "";
			System.out.println();
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				strDate = dateFormat.format(date);
				System.out.println("date date : " + strDate);
			}
			System.out.println("date date : " + strDate);
			f[4] = strDate;
			model.addRow(f);// lo inserto en tabla
		}

	}

	private void inicio() {
		animalSeleccionado=false;
		alimentoSeleccionado=false;
		animal = new Animal();
		alimento = new Alimento();
		lConsumeBorrar= new ArrayList<>();
		lConsume= new ArrayList<>();
		animalSeleccionado = false;
		alimentoSeleccionado = false;

		tableAnimal.setEnabled(true);
		btnSeleccionarAnimal.setEnabled(true);
		btnBuscarAnimal.setEnabled(true);
		btnSeleccionarAlimento.setEnabled(true);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldNombre.setEnabled(true);
		textFieldNombre.setText("");
		textFieldAlimentoGuardar.setText("");
		textFieldCantidad.setText("");
		comboBoxEspecie.setEnabled(true);
		comboBoxAlimento.setEnabled(true);
		textFieldAlimentoGuardar.setVisible(false);
		textFieldCantidad.setVisible(false);
		lblAlimentoInv.setVisible(false);
		lblCantidadInv.setVisible(false);
		btnGuardar.setVisible(false);

		DefaultTableModel model = (DefaultTableModel) tableAnimal.getModel();// borro tabla
		model.setRowCount(0);
		DefaultTableModel model2 = (DefaultTableModel) tableConsumos.getModel();// borro tabla
		model2.setRowCount(0);

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);

		comboBoxEspecie.removeAllItems();
		
		lEspecie = AppMain.per.extraerEspecie("");
		for (int i = 0; i < lEspecie.size(); i++) {
			comboBoxEspecie.addItem(lEspecie.get(i).getDescripcion());
		}

		lAlimento = AppMain.per.extraerAlimento("");
		for (int i = 0; i < lAlimento.size(); i++) {
			comboBoxAlimento.addItem(lAlimento.get(i).getDescripcion());
		}

	}
}
