package paqueteFinal;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import hibernate.Empleado;
import hibernate.Tratamiento;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class EmpleadosTratamientoGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Empleado> alEmpleadosTratamiento;
	List<Empleado> alEmpleadosDisponibles;
	List<Empleado> alEmpleadosBorrar;
	List<Tratamiento> alTratamiento;
	Tratamiento e;

	private JPanel contentPane;
	private JTable tableAsociados;
	private JTextField textFieldTratamiento;
	private JButton btnSeleccionar;
	private JButton btnBorrar;
	private JButton btnCancelar;
	private JButton btnConfirmar;
	private JTable tableNoAsociados;
	private JComboBox comboBoxTratamiento;
	private JButton buttonA�adir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmpleadosTratamientoGUI frame = new EmpleadosTratamientoGUI(padre);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public EmpleadosTratamientoGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Trabajadores por tratamiento");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 759, 461);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 126, 303, 247);
		contentPane.add(scrollPane);

		tableAsociados = new JTable();
		scrollPane.setViewportView(tableAsociados);

		btnSeleccionar = new JButton("Seleccionar tratamiento");
		btnSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarTratamiento();
			}
		});
		btnSeleccionar.setBounds(195, 10, 195, 23);
		contentPane.add(btnSeleccionar);

		JLabel lblEventoSeleccionado = new JLabel("Tratamiento seleccionado:");
		lblEventoSeleccionado.setBounds(10, 47, 175, 14);
		contentPane.add(lblEventoSeleccionado);

		textFieldTratamiento = new JTextField();
		textFieldTratamiento.setEnabled(false);
		textFieldTratamiento.setBounds(195, 44, 195, 20);
		contentPane.add(textFieldTratamiento);
		textFieldTratamiento.setColumns(10);

		btnBorrar = new JButton("Borrar empleado");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrarEmpleado();
			}
		});
		btnBorrar.setBounds(84, 384, 154, 29);
		contentPane.add(btnBorrar);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				confirmar();
			}
		});
		btnConfirmar.setEnabled(false);
		btnConfirmar.setBounds(426, 4, 132, 64);
		contentPane.add(btnConfirmar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		btnCancelar.setEnabled(false);
		btnCancelar.setBounds(581, 4, 132, 64);
		contentPane.add(btnCancelar);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(412, 126, 301, 245);
		contentPane.add(scrollPane_1);

		tableNoAsociados = new JTable();
		scrollPane_1.setViewportView(tableNoAsociados);
		tableNoAsociados.setFillsViewportHeight(true);

		buttonA�adir = new JButton("Aniadir empleado");
		buttonA�adir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				aniadirEmpleado();

			}
		});
		buttonA�adir.setEnabled(false);
		buttonA�adir.setBounds(499, 384, 154, 29);
		contentPane.add(buttonA�adir);

		JLabel lblEmpleadosDeLa = new JLabel("Empleados del tratamiento");
		lblEmpleadosDeLa.setBounds(10, 101, 172, 14);
		contentPane.add(lblEmpleadosDeLa);

		JLabel lblEmpleadosDisponibles = new JLabel("Empleados disponibles");
		lblEmpleadosDisponibles.setBounds(420, 101, 172, 14);
		contentPane.add(lblEmpleadosDisponibles);

		comboBoxTratamiento = new JComboBox();
		comboBoxTratamiento.setBounds(10, 11, 147, 20);
		contentPane.add(comboBoxTratamiento);

		//
		DefaultTableModel model = (DefaultTableModel) tableAsociados.getModel();
		DefaultTableModel model2 = (DefaultTableModel) tableNoAsociados.getModel();
		tableAsociados.setFillsViewportHeight(true);
		model.addColumn("ID");
		model.addColumn("Nombre");
		model.addColumn("Fecha");
		model.addColumn("Direccion");

		model2.addColumn("ID");
		model2.addColumn("Nombre");
		model2.addColumn("Fecha");
		model2.addColumn("Direccion");

		inicio();
		tableAsociados.setFillsViewportHeight(true);
		tableNoAsociados.setFillsViewportHeight(true);

		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				System.out.println("cerrando");
			}
		});

	}

	private void inicio() {
		DefaultTableModel model = (DefaultTableModel) tableAsociados.getModel();// borro tabla
		DefaultTableModel model2 = (DefaultTableModel) tableNoAsociados.getModel();// borro tabla
		model.setRowCount(0);
		model2.setRowCount(0);
		btnCancelar.setEnabled(false);
		btnConfirmar.setEnabled(false);
		btnBorrar.setEnabled(false);
		alTratamiento = AppMain.per.extraerTratamiento("");
		textFieldTratamiento.setText("");

		for (int i = 0; i < alTratamiento.size(); i++) {
			comboBoxTratamiento.addItem(alTratamiento.get(i).getDescripcion());
		}

	}

	protected void seleccionarTratamiento() {
		alEmpleadosBorrar = new ArrayList<>();
		alEmpleadosDisponibles = AppMain.per.extraerEmpleado("", "");
		alEmpleadosTratamiento = new ArrayList<>();
		DefaultTableModel model = (DefaultTableModel) tableAsociados.getModel();// borro tabla
		DefaultTableModel model2 = (DefaultTableModel) tableNoAsociados.getModel();// borro tabla
		model.setRowCount(0);
		model2.setRowCount(0);
		
		textFieldTratamiento.setText(alTratamiento.get(comboBoxTratamiento.getSelectedIndex()).getDescripcion());
		Tratamiento z = new Tratamiento();
		z.setDescripcion(textFieldTratamiento.getText());
		 z = AppMain.per.buscarTratamiento(z);
		
		
		e = AppMain.per.buscarTratamiento(alTratamiento.get(comboBoxTratamiento.getSelectedIndex()));
		AppMain.per.transaccionRefresh(e);

		Set setEmpleadosTratamiento = e.getEmpleados();
		System.out.println("set " + setEmpleadosTratamiento.size());
		if (setEmpleadosTratamiento.size() > 0) {
			alEmpleadosTratamiento = new ArrayList<>();
			alEmpleadosTratamiento.addAll(setEmpleadosTratamiento);
			alEmpleadosDisponibles = AppMain.per.extraerEmpleado("", "");
			if (alEmpleadosTratamiento.size() > 0) {
				alEmpleadosDisponibles.removeAll(alEmpleadosTratamiento);
			}
		}

		aniadirTabla();

		btnBorrar.setEnabled(true);
		buttonA�adir.setEnabled(true);

	}

	private void aniadirTabla() {
		if (alEmpleadosDisponibles == null) {
			JOptionPane.showMessageDialog(this, "No hay empleados disponibles.");
			return;
		}

		DefaultTableModel model = (DefaultTableModel) tableAsociados.getModel();
		DefaultTableModel model2 = (DefaultTableModel) tableNoAsociados.getModel();
		// borro tablas
		model.setRowCount(0);
		model2.setRowCount(0);
		// aniado empleados disponibles
		for (int i = 0; i < alEmpleadosDisponibles.size(); i++) {
			Object[] f = new Object[4];
			f[0] = alEmpleadosDisponibles.get(i).getId();// añado id
			f[1] = alEmpleadosDisponibles.get(i).getNombre();// añado descripcion

			Date date = alEmpleadosDisponibles.get(i).getFechaNac();
			String strDate = "";
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				strDate = dateFormat.format(date);
			}
			f[2] = strDate;

			f[3] = alEmpleadosDisponibles.get(i).getDireccion();// añado direccion
			model2.addRow(f);// lo inserto en tabla
		}

		// aniado empleados asociados
		if (alEmpleadosTratamiento != null) {
			for (int i = 0; i < alEmpleadosTratamiento.size(); i++) {
				Object[] f = new Object[4];
				f[0] = alEmpleadosTratamiento.get(i).getId();// añado id
				f[1] = alEmpleadosTratamiento.get(i).getNombre();// añado descripcion

				Date date = alEmpleadosTratamiento.get(i).getFechaNac();
				String strDate = "";
				if (date != null) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					strDate = dateFormat.format(date);
				}
				f[2] = strDate;

				f[3] = alEmpleadosTratamiento.get(i).getDireccion();// añado direccion
				model.addRow(f);// lo inserto en tabla
			}
			if (alEmpleadosTratamiento.size() > 0) {
				btnBorrar.setEnabled(true);
				btnConfirmar.setEnabled(true);
				btnCancelar.setEnabled(true);
			}
		}

	}

	protected void aniadirEmpleado() {
		
		
		if (tableNoAsociados.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(this, "Necesitas seleccionar un empleado para a�adirlo.");
			return;
		}
		//creo un empleado con el id seleccionado
		Empleado emp = new Empleado();
		emp.setId((alEmpleadosDisponibles.get(tableNoAsociados.getSelectedRow()).getId()));
		emp = AppMain.per.buscarEmpleado(emp);
		//lo meto en el list de empleados en el tratamiento
		alEmpleadosTratamiento.add(emp);
		//si el list de empleados a borrar lo tiene, se lo quita
		if (alEmpleadosBorrar.contains(emp)) {
			alEmpleadosBorrar.remove(emp);
		}
		//lo quito del list de empleados disponibles
		alEmpleadosDisponibles.remove(emp);
		
		aniadirTabla();
		btnConfirmar.setEnabled(true);
		btnCancelar.setEnabled(true);
		
		aniadirTabla();

	}

	protected void borrarEmpleado() {
		if (tableAsociados.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(this, "Necesitas seleccionar un empleado para borrarlo.");
			return;
		}
		
		//creo un empleado con el id seleccionado
				Empleado emp = new Empleado();
				emp.setId((alEmpleadosTratamiento.get(tableAsociados.getSelectedRow()).getId()));
				emp = AppMain.per.buscarEmpleado(emp);
				//lo aniado en el list de empleados disponibles
				alEmpleadosDisponibles.add(emp);
				//lo aniado en el list de empleados a borrar
				alEmpleadosBorrar.add(emp);
				//lo quito del list de empleados en el tratameinto
				alEmpleadosTratamiento.remove(emp);
				
				aniadirTabla();

	}

	protected void cancelar() {
		if (JOptionPane.showConfirmDialog(this,"�Est�s seguro que quieres cancelar?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		inicio();
		JOptionPane.showMessageDialog(this, "Acciones canceladas.");
		}

	}

	protected void confirmar() {
		if (JOptionPane.showConfirmDialog(this,"Est�s seguro que quieres confirmar?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
			Tratamiento x = new Tratamiento();
			x.setId(alTratamiento.get(comboBoxTratamiento.getSelectedIndex()).getId());
			x = AppMain.per.buscarTratamiento(x);
			 
			
		x.getEmpleados().clear();
		
		for (Empleado emp : alEmpleadosBorrar) {
			emp.getTratamientos().remove(x);
		}
		for (Empleado emp : alEmpleadosTratamiento) {
			emp.getTratamientos().add(x);
			x.getEmpleados().add(emp);
		}
		
		
			try {
				AppMain.per.guardarTratamiento(x);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

		inicio();
		JOptionPane.showMessageDialog(this, "Acciones confirmadas.");
		}
	}

}
