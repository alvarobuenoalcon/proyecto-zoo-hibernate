package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Zona;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ZonaGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Zona> alZona;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldZona;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ZonaGUI frame = new ZonaGUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param actionListener 
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public ZonaGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de ZONA");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 503, 377);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(173, 255, 47));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(66, 11, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(35, 14, 21, 14);
		contentPane.add(lblId);

		textFieldZona = new JTextField();
		textFieldZona.setColumns(10);
		textFieldZona.setBounds(66, 42, 86, 20);
		contentPane.add(textFieldZona);

		JLabel lblZona = new JLabel("ZONA");
		lblZona.setBounds(21, 45, 35, 14);
		contentPane.add(lblZona);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(179, 11, 89, 53);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(179, 75, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(63, 75, 89, 23);
		contentPane.add(btnBorrar);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 142, 419, 164);
		contentPane.add(scrollPane);
		
				table = new JTable();
				table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						btnGuardar.setEnabled(true);
						btnBorrar.setEnabled(true);
						try {
							actualizarFormulario();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
				});
				scrollPane.setViewportView(table);
				
						ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
				////
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				table.setDefaultEditor(Object.class, null);
				table.setFillsViewportHeight(true);

		JButton btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(290, 11, 164, 87);
		contentPane.add(btnLimpiar);
		model.addColumn("ID");
		model.addColumn("Zona");
	}

	protected void actualizarFormulario() {
		if (table.getSelectedRow() != -1) {
			System.out.println("size array:" + alZona.size());
			System.out.println("pos tabla " + table.getSelectedRow());
			textFieldZona.setText(alZona.get(table.getSelectedRow()).getDescripcion());
			textFieldID.setText(String.valueOf(alZona.get(table.getSelectedRow()).getId()));
			System.out.println("puesto");
		}

	}

	protected void borrar() {
		if (textFieldID.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No hay ninguna zona seleccionada para borrar.");
			return;
		}
		
		if (JOptionPane.showConfirmDialog(this,
				"Desea borrar la zona con id: " + textFieldID.getText() + "?", "CUIDADO",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
		
		String cadenaZona = textFieldZona.getText().toUpperCase();

		Zona z = new Zona();
		z.setId(Integer.valueOf(textFieldID.getText()));
		Zona z1 = AppMain.per.buscarZona(z);
		System.out.println("borrar a " + z1.getDescripcion());
		
		//Si la zona tiene animales asociados no se puede borrar
		if (!AppMain.per.buscarZona(z1).getAnimals().isEmpty()) {
			JOptionPane.showMessageDialog(this, "No puedes borrar la zona porque tiene animales asociados.");
			return;
		} 
		//Si la zona tiene empleados asociados no se puede borrar
		if (!AppMain.per.buscarZona(z1).getEmpleados().isEmpty()) {
			JOptionPane.showMessageDialog(this, "No puedes borrar la zona porque tiene empleados asociados.");
			return;
		} 
		
			AppMain.per.borrarZona(z1);
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnGuardar.setEnabled(false);
		btnBorrar.setEnabled(false);
		textFieldID.setText("");
		textFieldZona.setText("");
		JOptionPane.showMessageDialog(this, "La zona se ha borrado correctamente.");
		}
	}

	protected void guardar() {
		if (textFieldZona.getText().length()==0) {
			JOptionPane.showMessageDialog(this, "El campo de zona no puede estar vacío");
			return;
		}
		
		if (!textFieldID.getText().equals("")) {// compruebo que sea un id ya existente para update

			if (JOptionPane.showConfirmDialog(this,"Desea sobreescribir la zona con id: " + textFieldID.getText() + "?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Zona alExiste=AppMain.per.buscarZona(new Zona(textFieldZona.getText().trim().toUpperCase()));//compruebo si existe uno con el mismo nombre
				
				if (alExiste!=null && alExiste.getId()!=Integer.parseInt(textFieldID.getText())) {
					JOptionPane.showMessageDialog(this, "No se pudo sobreescribir la zona porque ya hay una con el mismo nombre.");
					return;
				}
				
				Zona alPrueba = new Zona();
				alPrueba.setId(Integer.valueOf(textFieldID.getText()));
				Zona al = AppMain.per.buscarZona(alPrueba);
				al.setDescripcion(textFieldZona.getText().trim().toUpperCase());
				
				try {
					AppMain.per.guardarZona(al);
					JOptionPane.showMessageDialog(this, "Zona guardada correctamente.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} else {// creo uno nuevo
			if (JOptionPane.showConfirmDialog(this,"Desea crear la zona  " + textFieldZona.getText() + "?", "CONFIRMAR",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Zona alExiste=AppMain.per.buscarZona(new Zona(textFieldZona.getText().trim().toUpperCase()));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null) {
					JOptionPane.showMessageDialog(this, "No se pudo guardar la zona porque ya hay una con el mismo nombre.");
					return;
				}
				Zona alPrueba = new Zona();
			alPrueba.setDescripcion(textFieldZona.getText().trim().toUpperCase());
			try {
				AppMain.per.guardarZona(alPrueba);
				JOptionPane.showMessageDialog(this, "Zona guardada correctamente.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			
		}
		inicio();
		
	}

	

	protected void buscar() {
		textFieldID.setText("");
		textFieldZona.setText(textFieldZona.getText().trim().toUpperCase());
		
			aniadirTabla();
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);


	}

	private void aniadirTabla() {
		alZona = AppMain.per.extraerZona(textFieldZona.getText());
		System.out.println(alZona.size());
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alZona.size(); i++) {
			Object[] f = new Object[2];
			f[0] = alZona.get(i).getId();// añado id
			f[1] = alZona.get(i).getDescripcion();// añado descripcion
			model.addRow(f);// lo inserto en tabla
		}

	}

	private void inicio() {

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldZona.setText("");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
	}
}
