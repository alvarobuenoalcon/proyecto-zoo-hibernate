package paqueteFinal;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import hibernate.Animaltratamiento;
import hibernate.Especie;
import hibernate.Nomina;

import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.RowSorter.SortKey;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;

public class Informe extends JFrame {
	static javax.swing.JFrame padre;
	private JPanel contentPane;
	private JTextField textFieldDesde;
	private JTextField textFieldHasta;
	private JTable table;
	private JComboBox comboBoxEspecie;
	
	List<Especie> lEspecie;
	List<Animaltratamiento> lTratPorEsp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Informe frame = new Informe(padre);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public Informe(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Listado de tratamientos por Especie");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 846, 290);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldDesde = new JTextField();
		textFieldDesde.setText("02/02/2002 22:22");
		textFieldDesde.setToolTipText("dd/MM/yyyy HH:mm");
		textFieldDesde.setBounds(126, 52, 121, 20);
		contentPane.add(textFieldDesde);
		textFieldDesde.setColumns(10);

		JLabel lblDesde = new JLabel("Desde:");
		lblDesde.setBounds(53, 55, 63, 14);
		contentPane.add(lblDesde);

		textFieldHasta = new JTextField();
		textFieldHasta.setToolTipText("dd/MM/yyyy HH:mm");
		textFieldHasta.setColumns(10);
		textFieldHasta.setBounds(126, 83, 121, 20);
		contentPane.add(textFieldHasta);

		JLabel lblHasta = new JLabel("Hasta:");
		lblHasta.setBounds(53, 86, 63, 14);
		contentPane.add(lblHasta);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscar();
			}
		});
		btnBuscar.setBounds(123, 126, 124, 23);
		contentPane.add(btnBuscar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(302, 24, 503, 125);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		comboBoxEspecie = new JComboBox();
		comboBoxEspecie.setBounds(127, 21, 120, 20);
		contentPane.add(comboBoxEspecie);

		JLabel lblEspecie = new JLabel("Especie:");
		lblEspecie.setBounds(53, 24, 63, 14);
		contentPane.add(lblEspecie);
		
		//////////
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.addColumn("FECHA");
		model.addColumn("ANIMAL");
		model.addColumn("TRATAMIENTO");
		model.addColumn("EMPLEADO");
		table.setFillsViewportHeight(true);
		
		inicio();
	}

	private void inicio() {
		 lEspecie = AppMain.per.extraerEspecie("");
		for (int i = 0; i < lEspecie.size(); i++) {
			comboBoxEspecie.addItem(lEspecie.get(i).getDescripcion());
		}
		
	}

	protected void buscar() {
		//especie
		Especie e =AppMain.per.buscarEspecie(new Especie (comboBoxEspecie.getSelectedItem().toString()));
		
		//desde
		if (textFieldDesde.getText().length() == 0) {
			JOptionPane.showMessageDialog(this, "El campo de fecha 'desde' esta vacio.");
			return;
		}
		Date fechaDesde = new Date();

		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			fechaDesde = dateFormat.parse(textFieldDesde.getText());
		} catch (ParseException ee) {
			JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy HH:mm");
			ee.printStackTrace();
			return;
		}
		
		//hasta
		if (textFieldHasta.getText().length() == 0) {
			JOptionPane.showMessageDialog(this, "El campo de fecha 'hasta' esta vacio.");
			return;
		}
		Date fechaHasta = new Date();

		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			fechaHasta = dateFormat.parse(textFieldHasta.getText());
		} catch (ParseException ee) {
			JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy HH:mm");
			ee.printStackTrace();
			return;
		}
		if (fechaDesde.before(fechaHasta)) {
			JOptionPane.showMessageDialog(this, "La fecha 'desde' debe ser anterior a la fecha 'hasta'");
			return;
		}
		lTratPorEsp = AppMain.per.extraerTratPorEspecie(fechaDesde, fechaHasta, e);
		
		
		actualizarTabla();
		
	}

	private void actualizarTabla() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		lTratPorEsp.sort(null);
	model.setRowCount(0);

	for (int i = 0; i < lTratPorEsp.size(); i++) {
		Object[] f = new Object[4];
		Date date = lTratPorEsp.get(i).getId().getFechaHora();
		String strDate = "";
		if (date != null) {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			strDate = dateFormat.format(date);
		}
		f[0] = strDate;
		
		f[1] = lTratPorEsp.get(i).getAnimal().getNombre();// añado descripcion
		f[2] = lTratPorEsp.get(i).getTratamiento().getDescripcion();
		f[3] = lTratPorEsp.get(i).getEmpleado().getNombre();
		
		model.addRow(f);// lo inserto en tabla
	}

	}
}
