package paqueteFinal;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import hibernate.Entrada;
import hibernate.Evento;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class EntradasGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Evento> alEventos;
	List<Entrada> alEntrada;

	private JPanel contentPane;
	private JTable table;
	private JTextField textFieldEvento;
	private JTextField textFieldVenta;
	private JButton btnSeleccionar;
	private JButton btnVender;
	private JComboBox comboBoxEvento;
	private JButton btnBorrarEntrada;
	private JButton btnCancelar;
	private JButton btnConfirmar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EntradasGUI frame = new EntradasGUI(padre);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public EntradasGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Proceso de ENTRADAS");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 759, 345);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(348, 53, 388, 247);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		//
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		table.setFillsViewportHeight(true);

		comboBoxEvento = new JComboBox();
		comboBoxEvento.setBounds(10, 11, 144, 20);
		contentPane.add(comboBoxEvento);

		btnSeleccionar = new JButton("Seleccionar evento");
		btnSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarEvento();
			}
		});
		btnSeleccionar.setBounds(167, 10, 144, 23);
		contentPane.add(btnSeleccionar);

		JLabel lblEventoSeleccionado = new JLabel("Evento seleccionado:");
		lblEventoSeleccionado.setBounds(10, 54, 122, 14);
		contentPane.add(lblEventoSeleccionado);

		textFieldEvento = new JTextField();
		textFieldEvento.setEnabled(false);
		textFieldEvento.setBounds(167, 51, 144, 20);
		contentPane.add(textFieldEvento);
		textFieldEvento.setColumns(10);

		btnBorrarEntrada = new JButton("Borrar entrada");
		btnBorrarEntrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrarEntrada();
			}
		});
		btnBorrarEntrada.setBounds(472, 7, 154, 29);
		contentPane.add(btnBorrarEntrada);

		JLabel lblEntradasAVender = new JLabel("Entradas a vender:");
		lblEntradasAVender.setBounds(10, 98, 144, 14);
		contentPane.add(lblEntradasAVender);

		textFieldVenta = new JTextField();
		textFieldVenta.setBounds(167, 95, 144, 20);
		contentPane.add(textFieldVenta);
		textFieldVenta.setColumns(10);

		btnVender = new JButton("Vender");
		btnVender.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				vender();
			}
		});
		btnVender.setBounds(167, 126, 144, 23);
		contentPane.add(btnVender);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				confirmar();
			}
		});
		btnConfirmar.setEnabled(false);
		btnConfirmar.setBounds(10, 215, 132, 64);
		contentPane.add(btnConfirmar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		btnCancelar.setEnabled(false);
		btnCancelar.setBounds(167, 215, 132, 64);
		contentPane.add(btnCancelar);
		model.addColumn("ID");
		model.addColumn("Evento");
		model.addColumn("Precio");

		inicio();
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent e)  
			{ 
				AppMain.per.transaccionRollback();
			
				System.out.println("cerrando");
			}
		});
		
	}

	protected void cancelar() {

		AppMain.per.transaccionRollback();
		inicio();
		JOptionPane.showMessageDialog(this, "Acciones canceladas.");
	
		
	}

	protected void confirmar() {
		AppMain.per.transaccionCommit();
		inicio();
		JOptionPane.showMessageDialog(this, "Acciones confirmadas.");
		
	}

	protected void borrarEntrada() {

		if (table.getSelectedRow()==-1) {
			JOptionPane.showMessageDialog(this, "Necesitas seleccionar una entrada para borrarla.");
			return;
		}
		
		
		AppMain.per.borrarSinCommit(alEntrada.get(table.getSelectedRow()));
		JOptionPane.showMessageDialog(this, "Entrada borrada con �xito.");
		
		actualizarDatos();

	}

	private void actualizarDatos() {
		Evento e = AppMain.per.buscarEvento(alEventos.get(comboBoxEvento.getSelectedIndex()));
		alEntrada = AppMain.per.extraerEntrada(String.valueOf(e.getId()));
		alEventos = AppMain.per.extraerEvento("");
		textFieldVenta.setText("");
		aniadirTabla();
		
	}

	protected void vender() {
		try {
			int nEntradas = Integer.parseInt(textFieldVenta.getText());
			if (nEntradas <= 0) {
				throw new NumberFormatException();
			} else {
				if (JOptionPane.showConfirmDialog(this, "�Desea vender  " + textFieldVenta.getText() + " entradas?",
						"CONFIRMAR", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

					for (int i = 0; i < nEntradas; i++) {
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						Date date = new Date(System.currentTimeMillis());
						AppMain.per
								.guardarSinCommit(new Entrada(alEventos.get(comboBoxEvento.getSelectedIndex()), date));
					}
					btnCancelar.setEnabled(true);
					btnConfirmar.setEnabled(true);
					JOptionPane.showMessageDialog(this, textFieldVenta.getText() + " entradas vendidas.");
					actualizarDatos();
				}
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "El num. entradas debe ser un entero mayor que 0.");
			e.printStackTrace();
		}

	}

	protected void seleccionarEvento() {
		textFieldEvento.setText(alEventos.get(comboBoxEvento.getSelectedIndex()).getDescripcion());
		Evento e = AppMain.per.buscarEvento(alEventos.get(comboBoxEvento.getSelectedIndex()));
		if (textFieldEvento.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No existe ning�n evento.");
			return;
		}
		alEntrada = AppMain.per.extraerEntrada(String.valueOf(e.getId()));
		aniadirTabla();

		btnVender.setEnabled(true);
		
		System.out.println(e.getEntradas().size());

	}

	private void aniadirTabla() {
		System.out.println(alEntrada.size() + "size de el alEntrada -aniadir-");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alEntrada.size(); i++) {
			Object[] f = new Object[3];
			f[0] = alEntrada.get(i).getId();// añado id
			f[1] = alEntrada.get(i).getEvento().getDescripcion();// añado descripcion

			Date date = alEntrada.get(i).getFechaHoraVenta();
			String strDate = "";
			System.out.println();
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				strDate = dateFormat.format(date);
				System.out.println("date date : " + strDate);
			}
			System.out.println("date date : " + strDate);
			f[2] = strDate;
			model.addRow(f);// lo inserto en tabla

			if (alEntrada.size() > 0) {
				btnBorrarEntrada.setEnabled(true);
				btnConfirmar.setEnabled(true);
				btnCancelar.setEnabled(true);
			}
		}

	}

	private void inicio() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnVender.setEnabled(false);
		btnCancelar.setEnabled(false);
		btnConfirmar.setEnabled(false);
		btnBorrarEntrada.setEnabled(false);
		comboBoxEvento.removeAllItems();

		alEventos = AppMain.per.extraerEvento("");
		System.out.println(alEventos.size());
		for (int i = 0; i < alEventos.size(); i++) {
			comboBoxEvento.addItem(alEventos.get(i).getDescripcion());
		}

	}
}
