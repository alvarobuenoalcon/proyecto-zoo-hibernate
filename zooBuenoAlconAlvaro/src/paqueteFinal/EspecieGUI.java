package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Especie;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EspecieGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Especie> alEspecie;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldEspecie;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EspecieGUI frame = new EspecieGUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param appMain 
	 */
	
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public EspecieGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de ESPECIES");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 548, 390);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(118, 11, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(87, 14, 21, 14);
		contentPane.add(lblId);

		textFieldEspecie = new JTextField();
		textFieldEspecie.setColumns(10);
		textFieldEspecie.setBounds(118, 42, 86, 20);
		contentPane.add(textFieldEspecie);

		JLabel lblZona = new JLabel("ESPECIE");
		lblZona.setBounds(35, 45, 73, 14);
		contentPane.add(lblZona);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(231, 11, 89, 53);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(231, 75, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(115, 75, 89, 23);
		contentPane.add(btnBorrar);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 142, 471, 164);
		contentPane.add(scrollPane);
		
				table = new JTable();
				table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						btnGuardar.setEnabled(true);
						btnBorrar.setEnabled(true);
						try {
							actualizarFormulario();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
				});
				scrollPane.setViewportView(table);
				
						ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
				////
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				table.setDefaultEditor(Object.class, null);

		JButton btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(342, 11, 164, 87);
		contentPane.add(btnLimpiar);
		model.addColumn("ID");
		model.addColumn("Especie");
		table.setFillsViewportHeight(true);
	}

	protected void actualizarFormulario() {
		if (table.getSelectedRow() != -1) {
			System.out.println("size array:" + alEspecie.size());
			System.out.println("pos tabla " + table.getSelectedRow());
			textFieldEspecie.setText(alEspecie.get(table.getSelectedRow()).getDescripcion());
			textFieldID.setText(String.valueOf(alEspecie.get(table.getSelectedRow()).getId()));
			System.out.println("puesto");
		}

	}

	protected void borrar() {
		if (textFieldID.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No hay ninguna especie seleccionada para borrar.");
			return;
		}

		if (JOptionPane.showConfirmDialog(this, "Desea borrar la zona con id: " + textFieldID.getText() + "?",
				"CUIDADO", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			String cadenaEspecie = textFieldEspecie.getText().toUpperCase();

			Especie e = new Especie();
			e.setId(Integer.valueOf(textFieldID.getText()));
			Especie e1 = AppMain.per.buscarEspecie(e);
			System.out.println("borrar a " + e1.getDescripcion());
			//Si la especie tiene animales asociados no se puede borrar
			if (!AppMain.per.buscarEspecie(e1).getAnimals().isEmpty()) {
				JOptionPane.showMessageDialog(this, "No puedes borrar la especie porque tiene animales asociados.");
				return;

			} 
			
			AppMain.per.borrarEspecie(e1);
			DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
			model.setRowCount(0);
			btnGuardar.setEnabled(false);
			btnBorrar.setEnabled(false);
			textFieldID.setText("");
			textFieldEspecie.setText("");
			JOptionPane.showMessageDialog(this, "La especie se ha borrado correctamente.");
		}
	}

	protected void guardar() {
		if (textFieldEspecie.getText().length()==0) {
			JOptionPane.showMessageDialog(this, "El campo de especie no puede estar vacío");
			return;
		}
		
		if (!textFieldID.getText().equals("")) {// compruebo que sea un id ya existente para update

			if (JOptionPane.showConfirmDialog(this,"Desea sobreescribir la especie con id: " + textFieldID.getText() + "?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Especie alExiste=AppMain.per.buscarEspecie(new Especie(textFieldEspecie.getText().trim().toUpperCase()));//compruebo si existe uno con el mismo nombre
				
				if (alExiste!=null && alExiste.getId()!=Integer.parseInt(textFieldID.getText())) {
					JOptionPane.showMessageDialog(this, "No se pudo sobreescribir la especie porque ya hay una con el mismo nombre.");
					return;
				}
				
				Especie alPrueba = new Especie();
				alPrueba.setId(Integer.valueOf(textFieldID.getText()));
				Especie al = AppMain.per.buscarEspecie(alPrueba);
				al.setDescripcion(textFieldEspecie.getText().trim().toUpperCase());
				
				try {
					AppMain.per.guardarEspecie(al);
					JOptionPane.showMessageDialog(this, "Especie guardada correctamente.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} else {// creo uno nuevo
			if (JOptionPane.showConfirmDialog(this,"Desea crear la especie  " + textFieldEspecie.getText().trim().toUpperCase() + "?", "CONFIRMAR",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Especie alExiste=AppMain.per.buscarEspecie(new Especie(textFieldEspecie.getText().trim().toUpperCase()));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null) {
					JOptionPane.showMessageDialog(this, "No se pudo guardar la especie porque ya hay una con el mismo nombre.");
					return;
				}
				Especie alPrueba = new Especie();
			alPrueba.setDescripcion(textFieldEspecie.getText().trim().toUpperCase());
			try {
				AppMain.per.guardarEspecie(alPrueba);
				JOptionPane.showMessageDialog(this, "Especie guardada correctamente.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}

			
		}
		inicio();
	}

	protected void buscar() {
		textFieldID.setText("");
		textFieldEspecie.setText(textFieldEspecie.getText().trim().toUpperCase());
		
			aniadirTabla();
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);

		

	}

	private void aniadirTabla() {
		alEspecie = AppMain.per.extraerEspecie(textFieldEspecie.getText());
		System.out.println(textFieldEspecie.getText()+"texttttt");
		System.out.println(alEspecie.size()+"size");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alEspecie.size(); i++) {
			Object[] f = new Object[2];
			f[0] = alEspecie.get(i).getId();// añado id
			f[1] = alEspecie.get(i).getDescripcion();// añado descripcion
			model.addRow(f);// lo inserto en tabla
		}

	}

	private void inicio() {

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldEspecie.setText("");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
	}
}
