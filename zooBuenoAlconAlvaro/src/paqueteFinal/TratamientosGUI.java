package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Tratamiento;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TratamientosGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Tratamiento> alTratamientos;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldTratamiento;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JTextField textFieldCoste;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TratamientosGUI frame = new TratamientosGUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public TratamientosGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de TRATAMIENTOS");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 548, 390);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(143, 188, 143));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(118, 11, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(10, 14, 21, 14);
		contentPane.add(lblId);

		textFieldTratamiento = new JTextField();
		textFieldTratamiento.setColumns(10);
		textFieldTratamiento.setBounds(118, 42, 86, 20);
		contentPane.add(textFieldTratamiento);

		JLabel lblZona = new JLabel("TRATAMIENTOS");
		lblZona.setBounds(10, 45, 98, 14);
		contentPane.add(lblZona);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(231, 11, 89, 53);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(231, 112, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(118, 112, 89, 23);
		contentPane.add(btnBorrar);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 176, 471, 164);
		contentPane.add(scrollPane);
		
				table = new JTable();
				table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						btnGuardar.setEnabled(true);
						btnBorrar.setEnabled(true);

						try {
							actualizarFormulario();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
				});
				scrollPane.setViewportView(table);
				
						ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
				////
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				table.setDefaultEditor(Object.class, null);
				table.setFillsViewportHeight(true);

		JButton btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(342, 11, 164, 87);
		contentPane.add(btnLimpiar);

		textFieldCoste = new JTextField();
		textFieldCoste.setColumns(10);
		textFieldCoste.setBounds(118, 73, 86, 20);
		contentPane.add(textFieldCoste);

		JLabel lblCoste = new JLabel("COSTE");
		lblCoste.setBounds(8, 70, 73, 14);
		contentPane.add(lblCoste);
		model.addColumn("ID");
		model.addColumn("Alimento");
		model.addColumn("Coste");
	}

	protected void borrar() {

		if (textFieldID.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No hay ningun tratamiento seleccionado para borrar.");
			return;
		}

		if (JOptionPane.showConfirmDialog(this, "Desea borrar el tratamiento con id: " + textFieldID.getText() + "?",
				"CUIDADO", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
			String cadenaEspecie = textFieldTratamiento.getText().toUpperCase();
			Tratamiento e = new Tratamiento();
			e.setId(Integer.valueOf(textFieldID.getText()));
			Tratamiento e1 = AppMain.per.buscarTratamiento(e);
			System.out.println("borrar a " + e1.getDescripcion());
			

			if (!e1.getAnimaltratamientos().isEmpty() || !e1.getEmpleados().isEmpty()) {
				JOptionPane.showMessageDialog(this, "El tratamiento no puede ser borrado porque tiene animales tratados o empleados asociados.");
				return;
			}
			
			AppMain.per.borrarTratamiento(e1);
			DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
			model.setRowCount(0);
			btnGuardar.setEnabled(false);
			btnBorrar.setEnabled(false);
			textFieldID.setText("");
			textFieldTratamiento.setText("");
			textFieldCoste.setText("");
			JOptionPane.showMessageDialog(this, "El tratamiento se ha borrado correctamente.");
		}

	}

	protected void guardar() {
		if (textFieldTratamiento.getText().length()==0) {
			JOptionPane.showMessageDialog(this, "El campo de tratamiento no puede estar vacío");
			return;
		}
		Double coste = 0.0;
		if (textFieldCoste.getText().equals("")) {
			textFieldCoste.setText("0.0");
		}
		try { // COMPRUEBO QUE SEA UN DOUBLE EL COSTE
			coste = Double.valueOf(textFieldCoste.getText());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "El formato de coste debe ser: NUM.DEC");
			e.printStackTrace();
			return;

		}
		if (Double.valueOf(textFieldCoste.getText())<0) {
			JOptionPane.showMessageDialog(this, "El coste no puede ser negativo");
			return;
		}
		
		

		if (!textFieldID.getText().equals("")) {// compruebo que sea un id ya existente para update

			if (JOptionPane.showConfirmDialog(this,"Desea sobreescribir el tratamiento con id: " + textFieldID.getText() + "?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Tratamiento alExiste=AppMain.per.buscarTratamiento(new Tratamiento(textFieldTratamiento.getText().trim().toUpperCase(),0.0));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null && alExiste.getId()!=Integer.parseInt(textFieldID.getText())) {
					JOptionPane.showMessageDialog(this, "No se pudo sobreescribir el tratamiento porque ya hay uno con el mismo nombre.");
					return;
				}
				
				Tratamiento alPrueba = new Tratamiento();
				alPrueba.setId(Integer.valueOf(textFieldID.getText()));
				Tratamiento al = AppMain.per.buscarTratamiento(alPrueba);
				al.setDescripcion(textFieldTratamiento.getText().trim().toUpperCase());
				al.setCoste(coste);
				try {
					AppMain.per.guardarTratamiento(al);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} else {// creo uno nuevo
			if (JOptionPane.showConfirmDialog(this,"Desea crear el tratamiento  " + textFieldTratamiento.getText().trim().toUpperCase() + "?", "CONFIRMAR",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Tratamiento alExiste=AppMain.per.buscarTratamiento(new Tratamiento(textFieldTratamiento.getText().trim().toUpperCase(),0.0));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null) {
					JOptionPane.showMessageDialog(this, "No se pudo guardar el tratamiento porque ya hay uno con el mismo nombre.");
					return;
				}
				Tratamiento alPrueba = new Tratamiento();
			alPrueba.setCoste(coste);
			alPrueba.setDescripcion(textFieldTratamiento.getText().trim().toUpperCase());
			try {
				AppMain.per.guardarTratamiento(alPrueba);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		}
		inicio();
		JOptionPane.showMessageDialog(this, "Tratamiento guardado correctamente.");
	}

	protected void buscar() {
		textFieldCoste.setText("");
		textFieldID.setText("");
		textFieldTratamiento.setText(textFieldTratamiento.getText().trim().toUpperCase());
		
			aniadirTabla();
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);


	}

	private void aniadirTabla() {
		alTratamientos = AppMain.per.extraerTratamiento(textFieldTratamiento.getText());
		System.out.println(alTratamientos.size());
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alTratamientos.size(); i++) {
			Object[] f = new Object[3];
			f[0] = alTratamientos.get(i).getId();// añado id
			f[1] = alTratamientos.get(i).getDescripcion();// añado descripcion
			f[2] = alTratamientos.get(i).getCoste();
			model.addRow(f);// lo inserto en tabla
		}

	}

	protected void actualizarFormulario() {
		if (table.getSelectedRow() != -1) {
			System.out.println("size array:" + alTratamientos.size());
			System.out.println("pos tabla " + table.getSelectedRow());
			textFieldTratamiento.setText(alTratamientos.get(table.getSelectedRow()).getDescripcion());
			textFieldID.setText(String.valueOf(alTratamientos.get(table.getSelectedRow()).getId()));
			textFieldCoste.setText(String.valueOf(alTratamientos.get(table.getSelectedRow()).getCoste()));
			System.out.println("puesto");
		}

	}

	private void inicio() {

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldTratamiento.setText("");
		textFieldCoste.setText("");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
	}
}
