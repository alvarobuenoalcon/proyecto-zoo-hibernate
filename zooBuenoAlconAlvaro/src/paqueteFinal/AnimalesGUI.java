package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Animal;
import hibernate.Especie;
import hibernate.Zona;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AnimalesGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Animal> alAnimales;
	List<Especie> lEspecie;
	List<Zona> lZona;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldNombre;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JComboBox comboBoxEspecie;
	private JComboBox comboBoxZona;
	private JTextField textFieldFecha;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AnimalesGUI frame = new AnimalesGUI(padre);
					
					frame.setVisible(true);
					

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param appMain 
	 */
	public AnimalesGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de ANIMALES");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 840, 541);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(143, 188, 143));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(118, 11, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(45, 14, 21, 14);
		contentPane.add(lblId);

		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(118, 42, 86, 20);
		contentPane.add(textFieldNombre);

		JLabel lblZona = new JLabel("NOMBRE");
		lblZona.setBounds(45, 45, 54, 14);
		contentPane.add(lblZona);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(231, 11, 89, 53);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(231, 92, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(231, 135, 89, 23);
		contentPane.add(btnBorrar);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 281, 779, 210);
		contentPane.add(scrollPane);
		
				table = new JTable();
				table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						btnGuardar.setEnabled(true);
						btnBorrar.setEnabled(true);

						try {
							actualizarFormulario();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
				scrollPane.setViewportView(table);
				
						ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		table.setDefaultEditor(Object.class, null);

		JButton btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(342, 11, 164, 104);
		contentPane.add(btnLimpiar);

		JLabel lblCoste = new JLabel("ESPECIE");
		lblCoste.setBounds(45, 76, 73, 14);
		contentPane.add(lblCoste);
		
		JLabel lblZona_1 = new JLabel("ZONA");
		lblZona_1.setBounds(45, 107, 54, 14);
		contentPane.add(lblZona_1);
		
		comboBoxZona = new JComboBox();
		comboBoxZona.setBounds(118, 104, 86, 20);
		contentPane.add(comboBoxZona);
		
		
		JLabel lblFechaNac = new JLabel("FECHA NAC");
		lblFechaNac.setBounds(45, 139, 73, 14);
		contentPane.add(lblFechaNac);
		
		comboBoxEspecie = new JComboBox();
		comboBoxEspecie.setBounds(118, 73, 86, 20);
		contentPane.add(comboBoxEspecie);

		
		textFieldFecha = new JTextField();
		textFieldFecha.setToolTipText("DD/MM/YYYY");
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(118, 136, 86, 20);
		contentPane.add(textFieldFecha);

		////
		inicio();
		model.addColumn("ID");
		model.addColumn("Nombre");
		model.addColumn("Especie");
		model.addColumn("Zona");
		model.addColumn("FechaNac");
		table.setFillsViewportHeight(true);
	}

	protected void borrar() {

		if (textFieldID.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No hay ningun animal seleccionado para borrar.");
			return;
		}

		if (JOptionPane.showConfirmDialog(this, "Desea borrar el animal con id: " + textFieldID.getText() + "?",
				"CUIDADO", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			String cadenaEspecie = textFieldNombre.getText().toUpperCase();
			Animal e = new Animal();
			e.setId(Integer.valueOf(textFieldID.getText()));
			Animal e1 = AppMain.per.buscarAnimal(e);
			
			if (!e1.getConsumes().isEmpty() || !e1.getAnimaltratamientos().isEmpty()) {
				JOptionPane.showMessageDialog(this, "El Animal no se ha podido borrar porque tiene tratamientos o alimentos asociados.");
				return;
			}
			
			System.out.println("borrar a " + e1.getNombre());
			AppMain.per.borrarAnimal(e1);
			DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
			model.setRowCount(0);
			btnGuardar.setEnabled(false);
			btnBorrar.setEnabled(false);
			textFieldID.setText("");
			textFieldNombre.setText("");
			
			JOptionPane.showMessageDialog(this, "El Animal se ha borrado correctamente.");
		}

	}

	protected void guardar() {
		if (textFieldNombre.getText().length()==0) {
			JOptionPane.showMessageDialog(this, "El campo de nombre no puede estar vacío");
			return;
		}
		
		if (!textFieldID.getText().equals("")) {// compruebo que sea un id ya existente para update

			if (JOptionPane.showConfirmDialog(this,"Desea sobreescribir el animal con id: " + textFieldID.getText() + "?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Animal ejemplo = new Animal();
				ejemplo.setNombre(textFieldNombre.getText().toUpperCase().trim());
				ejemplo.setEspecie(AppMain.per.buscarEspecie(new Especie (comboBoxEspecie.getSelectedItem().toString())));
				ejemplo.setZona(AppMain.per.buscarZona(new Zona(comboBoxZona.getSelectedItem().toString())));
				
				Animal alExiste=AppMain.per.buscarAnimal(ejemplo);//compruebo si existe uno con el mismo nombre
				if (alExiste!=null && alExiste.getId()!=Integer.parseInt(textFieldID.getText())) {
					JOptionPane.showMessageDialog(this, "No se pudo sobreescribir el animal porque ya hay uno con el mismo nombre.");
					return;
				}
				ejemplo.setId(Integer.parseInt(textFieldID.getText()));
				alExiste=AppMain.per.buscarAnimal(ejemplo);
				try {
					
					System.out.println("aaaaAAAAAAA"+alExiste.getId()+"AAAAAAAAAAAAAAAAA");
					alExiste.setNombre(textFieldNombre.getText().toUpperCase().trim());
					alExiste.setEspecie(AppMain.per.buscarEspecie(new Especie (comboBoxEspecie.getSelectedItem().toString())));
					alExiste.setZona(AppMain.per.buscarZona(new Zona(comboBoxZona.getSelectedItem().toString())));
					
					
					if (textFieldFecha.getText().length()>0) {
						System.out.println(textFieldFecha.getText()+" "+ textFieldFecha.getText().length());
						DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
						 try {
							alExiste.setFechaNac(dateFormat.parse(textFieldFecha.getText().trim()));
						} catch (ParseException e) {
							JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy");
							e.printStackTrace();
							return;
						}
					}else {
						alExiste.setFechaNac(null);
					}
					
					AppMain.per.guardarAnimal(alExiste);
					JOptionPane.showMessageDialog(this, "Animal guardado correctamente.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} else {// creo uno nuevo
			if (JOptionPane.showConfirmDialog(this,"Desea crear el animal  " + textFieldNombre.getText().trim().toUpperCase() + "?", "CONFIRMAR",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Animal ejemplo = new Animal();
				ejemplo.setNombre(textFieldNombre.getText().toUpperCase().trim());
				ejemplo.setEspecie(AppMain.per.buscarEspecie(new Especie (comboBoxEspecie.getSelectedItem().toString())));
				ejemplo.setZona(AppMain.per.buscarZona(new Zona(comboBoxZona.getSelectedItem().toString())));
				
				System.out.println(textFieldFecha.getText().length()+"    fecha");
				if (textFieldFecha.getText().length()>0) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
					 try {
						ejemplo.setFechaNac(dateFormat.parse(textFieldFecha.getText().trim()));
					} catch (ParseException e) {
						JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: DD/MM/YYYY");
						e.printStackTrace();
						return;
					}
				}
				
				
				Animal alExiste=AppMain.per.buscarAnimal(ejemplo);//compruebo si existe uno con el mismo nombre
				if (alExiste!=null && alExiste.getEspecie().getDescripcion().equals(ejemplo.getEspecie().getDescripcion())) {
					JOptionPane.showMessageDialog(this, "No se pudo guardar el animal porque ya hay uno con el mismo nombre.");
					return;
				}
				
			try {
				AppMain.per.guardarAnimal(ejemplo);
				JOptionPane.showMessageDialog(this, "Animal guardado correctamente.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
			
		}
		inicio();
	}

	protected void buscar() {
		
		textFieldFecha.setText("");
		textFieldID.setText("");
		textFieldNombre.setText(textFieldNombre.getText().trim().toUpperCase());
		

		 //if
			aniadirTabla();
			
			
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);
		
		
		
	}

	private void aniadirTabla() {
		alAnimales = AppMain.per.extraerAnimal(textFieldNombre.getText().toUpperCase().trim(), comboBoxEspecie.getSelectedItem().toString());
		System.out.println(alAnimales.size());
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alAnimales.size(); i++) {
			Object[] f = new Object[5];
			f[0] = alAnimales.get(i).getId();// añado id
			f[1] = alAnimales.get(i).getNombre();// añado descripcion
			f[2] = alAnimales.get(i).getEspecie().getDescripcion();
			f[3] = alAnimales.get(i).getZona().getDescripcion();
			
			Date date = alAnimales.get(i).getFechaNac(); 
			String strDate="";
			System.out.println();
			if (date!=null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
				 strDate = dateFormat.format(date);
				 System.out.println("date date : "+strDate);
			}
			System.out.println("date date : "+strDate);
			f[4] = strDate;
			model.addRow(f);// lo inserto en tabla
		}

	}

	protected void actualizarFormulario() {
		
		if (table.getSelectedRow() != -1) {
			
			Date date = alAnimales.get(table.getSelectedRow()).getFechaNac(); 
			String strDate="";
			if (date!=null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
				 strDate = dateFormat.format(date);
			}
			
			textFieldNombre.setText(alAnimales.get(table.getSelectedRow()).getNombre());
			textFieldID.setText(String.valueOf(alAnimales.get(table.getSelectedRow()).getId()));
			textFieldFecha.setText(strDate);
			//ewfewfwf
			for (int i = 0; i < lEspecie.size(); i++) {
				if (alAnimales.get(table.getSelectedRow()).getEspecie().equals(lEspecie.get(i))) {
					comboBoxEspecie.setSelectedIndex(i);
				}
			}
			
				for (int i = 0; i < lZona.size(); i++) {
					if (alAnimales.get(table.getSelectedRow()).getZona().equals(lZona.get(i))) {
						comboBoxZona.setSelectedIndex(i);
					}
				}
		}

	}

	private void inicio() {
		
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldNombre.setText("");
		textFieldFecha.setText("");
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		
		comboBoxEspecie.removeAllItems();
		comboBoxZona.removeAllItems();
		
		lEspecie = AppMain.per.extraerEspecie("");
		for (int i = 0; i < lEspecie.size(); i++) {
			comboBoxEspecie.addItem(lEspecie.get(i).getDescripcion());
		}
		
		lZona = AppMain.per.extraerZona("");
		for (int i = 0; i < lZona.size(); i++) {
			comboBoxZona.addItem(lZona.get(i).getDescripcion());
		}
		
	}
}
