package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Alimento;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AlimentosGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Alimento> alAlimentos;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldAlimento;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JTextField textFieldCoste;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlimentosGUI frame = new AlimentosGUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param appMain 
	 */
	
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public AlimentosGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de ALIMENTOS");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 548, 390);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 160, 122));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(118, 11, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(35, 14, 21, 14);
		contentPane.add(lblId);

		textFieldAlimento = new JTextField();
		textFieldAlimento.setColumns(10);
		textFieldAlimento.setBounds(118, 42, 86, 20);
		contentPane.add(textFieldAlimento);

		JLabel lblZona = new JLabel("ALIMENTOS");
		lblZona.setBounds(35, 45, 73, 14);
		contentPane.add(lblZona);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(231, 11, 89, 53);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(231, 112, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(118, 112, 89, 23);
		contentPane.add(btnBorrar);
	
		scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 176, 471, 164);
		contentPane.add(scrollPane);
		
				table = new JTable();
				table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						btnGuardar.setEnabled(true);
						btnBorrar.setEnabled(true);
		
						try {
							actualizarFormulario();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
				scrollPane.setViewportView(table);
				
						ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
				////
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				table.setDefaultEditor(Object.class, null);

		JButton btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(342, 11, 164, 87);
		contentPane.add(btnLimpiar);

		textFieldCoste = new JTextField();
		textFieldCoste.setColumns(10);
		textFieldCoste.setBounds(118, 73, 86, 20);
		contentPane.add(textFieldCoste);

		JLabel lblCoste = new JLabel("COSTE");
		lblCoste.setBounds(35, 76, 73, 14);
		contentPane.add(lblCoste);
		model.addColumn("ID");
		model.addColumn("Alimento");
		model.addColumn("Coste");
		table.setFillsViewportHeight(true);
	}

	

	protected void borrar() {

		if (textFieldID.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No hay ningun alimento seleccionado para borrar.");
			return;
		}

		if (JOptionPane.showConfirmDialog(this, "Desea borrar el alimento con id: " + textFieldID.getText() + "?",
				"CUIDADO", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			String cadenaEspecie = textFieldAlimento.getText().toUpperCase();

			Alimento e = new Alimento();
			e.setId(Integer.valueOf(textFieldID.getText()));
			Alimento e1 = AppMain.per.buscarAlimento(e);
			System.out.println("borrar a " + e1.getDescripcion());
			
			//Si el alimento tiene animales asociados no se puede borrar
			if (!AppMain.per.buscarAlimento(e1).getConsumes().isEmpty()) {
				JOptionPane.showMessageDialog(this, "No puedes borrar el alimento porque tiene animales asociados.");
				return;
			}
			
			AppMain.per.borrarAlimento(e1);
			DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
			model.setRowCount(0);
			btnGuardar.setEnabled(false);
			btnBorrar.setEnabled(false);
			textFieldID.setText("");
			textFieldAlimento.setText("");
			textFieldCoste.setText("");
			JOptionPane.showMessageDialog(this, "El alimento se ha borrado correctamente.");
		}

	}

	protected void guardar() {
		if (textFieldAlimento.getText().length()==0) {
			JOptionPane.showMessageDialog(this, "El campo de alimento no puede estar vacío");
			return;
		}
		Double coste = 0.0;
		if (textFieldCoste.getText().equals("")) {
			textFieldCoste.setText("0.0");
		}
		try { // COMPRUEBO QUE SEA UN DOUBLE EL COSTE
			coste = Double.valueOf(textFieldCoste.getText());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "El formato de coste debe ser: NUM.DEC");
			e.printStackTrace();
			return;

		}
		if (Double.valueOf(textFieldCoste.getText())<0) {
			JOptionPane.showMessageDialog(this, "El coste no puede ser negativo");
			return;
		}
		
		

		if (!textFieldID.getText().equals("")) {// compruebo que sea un id ya existente para update

			if (JOptionPane.showConfirmDialog(this,"Desea sobreescribir el alimento con id: " + textFieldID.getText() + "?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Alimento alExiste=AppMain.per.buscarAlimento(new Alimento(textFieldAlimento.getText().trim().toUpperCase(),0.0));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null && alExiste.getId()!=Integer.parseInt(textFieldID.getText())) {
					JOptionPane.showMessageDialog(this, "No se pudo sobreescribir el alimento porque ya hay uno con el mismo nombre.");
					return;
				}
				
				Alimento alPrueba = new Alimento();
				alPrueba.setId(Integer.valueOf(textFieldID.getText()));
				Alimento al = AppMain.per.buscarAlimento(alPrueba);
				al.setDescripcion(textFieldAlimento.getText().trim().toUpperCase());
				al.setCoste(coste);
				try {
					AppMain.per.guardarAlimento(al);
					JOptionPane.showMessageDialog(this, "Alimento guardado correctamente.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} else {// creo uno nuevo
			if (JOptionPane.showConfirmDialog(this,"Desea crear el alimento  " + textFieldAlimento.getText().trim().toUpperCase() + "?", "CONFIRMAR",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Alimento alExiste=AppMain.per.buscarAlimento(new Alimento(textFieldAlimento.getText().trim().toUpperCase(),0.0));//compruebo si existe uno con el mismo nombre
				if (alExiste!=null) {
					JOptionPane.showMessageDialog(this, "No se pudo guardar el alimento porque ya hay uno con el mismo nombre.");
					return;
				}
			Alimento alPrueba = new Alimento();
			alPrueba.setCoste(coste);
			alPrueba.setDescripcion(textFieldAlimento.getText().trim().toUpperCase());
			try {
				AppMain.per.guardarAlimento(alPrueba);
				JOptionPane.showMessageDialog(this, "Alimento guardado correctamente.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			
		}
		inicio();
		
	}

	

	protected void buscar() {
		textFieldCoste.setText("");
		textFieldID.setText("");
		textFieldAlimento.setText(textFieldAlimento.getText().trim().toUpperCase());
	

			aniadirTabla();
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);


	}

	private void aniadirTabla() {
		alAlimentos = AppMain.per.extraerAlimento(textFieldAlimento.getText());
		System.out.println(alAlimentos.size());
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alAlimentos.size(); i++) {
			Object[] f = new Object[3];
			f[0] = alAlimentos.get(i).getId();// añado id
			f[1] = alAlimentos.get(i).getDescripcion();// añado descripcion
			f[2] = alAlimentos.get(i).getCoste();
			model.addRow(f);// lo inserto en tabla
		}

	}

	protected void actualizarFormulario() {
		if (table.getSelectedRow() != -1) {
			System.out.println("size array:" + alAlimentos.size());
			System.out.println("pos tabla " + table.getSelectedRow());
			textFieldAlimento.setText(alAlimentos.get(table.getSelectedRow()).getDescripcion());
			textFieldID.setText(String.valueOf(alAlimentos.get(table.getSelectedRow()).getId()));
			textFieldCoste.setText(String.valueOf(alAlimentos.get(table.getSelectedRow()).getCoste()));
			System.out.println("puesto");
		}

	}

	private void inicio() {

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldAlimento.setText("");
		textFieldCoste.setText("");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
	}
}
