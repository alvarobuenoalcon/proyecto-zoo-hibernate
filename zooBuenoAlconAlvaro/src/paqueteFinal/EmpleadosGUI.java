package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Empleado;
import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;



public class EmpleadosGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Empleado> alEmpleado;
	

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldNombre;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JTextField textFieldFecha;
	private JTextField textFieldDireccion;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmpleadosGUI frame = new EmpleadosGUI(padre);
					
					frame.setVisible(true);
					

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EmpleadosGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de EMPLEADOS");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 840, 541);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(238, 232, 170));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(118, 11, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(45, 14, 21, 14);
		contentPane.add(lblId);

		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(118, 42, 86, 20);
		contentPane.add(textFieldNombre);

		JLabel lblZona = new JLabel("NOMBRE");
		lblZona.setBounds(45, 45, 54, 14);
		contentPane.add(lblZona);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(231, 11, 89, 53);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(231, 92, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(231, 135, 89, 23);
		contentPane.add(btnBorrar);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 281, 779, 210);
		contentPane.add(scrollPane);
		
				table = new JTable();
				table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {

						try {
							actualizarFormulario();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					
					}
				});
				
				scrollPane.setViewportView(table);
				
						ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		table.setDefaultEditor(Object.class, null);

		JButton btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(342, 11, 164, 104);
		contentPane.add(btnLimpiar);
		
		
		JLabel lblFechaNac = new JLabel("FECHA NAC");
		lblFechaNac.setBounds(45, 76, 73, 14);
		contentPane.add(lblFechaNac);

		
		textFieldFecha = new JTextField();
		textFieldFecha.setToolTipText("DD/MM/YYYY");
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(118, 73, 86, 20);
		contentPane.add(textFieldFecha);
		
		textFieldDireccion = new JTextField();
		textFieldDireccion.setToolTipText("");
		textFieldDireccion.setText("");
		textFieldDireccion.setColumns(10);
		textFieldDireccion.setBounds(118, 101, 86, 20);
		contentPane.add(textFieldDireccion);
		
		JLabel lblDireccin = new JLabel("DIRECCION");
		lblDireccin.setBounds(45, 104, 73, 14);
		contentPane.add(lblDireccin);

		////
		inicio();
		model.addColumn("ID");
		model.addColumn("Nombre");
		model.addColumn("Direccion");
		model.addColumn("FechaNac");
		
		table.setFillsViewportHeight(true);
	}

	protected void borrar() {
		
		
		if (textFieldID.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "No hay ningun empleado seleccionado para borrar.");
			return;
		}
		
		Empleado e = new Empleado();
		e.setId(Integer.valueOf(textFieldID.getText()));
		Empleado e1 = AppMain.per.buscarEmpleado(e);
		
		if (e1.getZonas().size()>0) {
			JOptionPane.showMessageDialog(this, "No puedes borrar el empleado porque tiene zonas asociadas.");
			return;
		}
		
		if (e1.getNominas().size()>0) {
			JOptionPane.showMessageDialog(this, "No puedes borrar el empleado porque tiene nominas asociadas.");
			return;
		}
		if (e1.getAnimaltratamientos().size()>0) {
			JOptionPane.showMessageDialog(this, "No puedes borrar el empleado porque tiene animales siendo tratados asociados.");
			return;
		}
		if (e1.getTratamientos().size()>0) {
			JOptionPane.showMessageDialog(this, "No puedes borrar el empleado porque tiene tratamientos capacitados asociados.");
			return;
		}

		if (JOptionPane.showConfirmDialog(this, "Desea borrar el empleado con id: " + textFieldID.getText() + "?",
				"CUIDADO", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

			
			System.out.println("borrar a " + e1.getNombre());
			AppMain.per.borrarEmpleado(e1);
			DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
			model.setRowCount(0);
			btnGuardar.setEnabled(false);
			btnBorrar.setEnabled(false);
			textFieldID.setText("");
			textFieldNombre.setText("");
			textFieldDireccion.setText("");
			textFieldFecha.setText("");
			
			JOptionPane.showMessageDialog(this, "El empleado se ha borrado correctamente.");
		}

	}

	protected void guardar() {
		if (textFieldNombre.getText().length()<=0) {
			JOptionPane.showMessageDialog(this, "El campo nombre no puede estar vacío.");
			return;
		}
		if (textFieldFecha.getText().length()<=0) {
			JOptionPane.showMessageDialog(this, "El campo fecha no puede estar vacío.");
			return;
		}
		if (textFieldDireccion.getText().length()<=0) {
			JOptionPane.showMessageDialog(this, "El campo dirección no puede estar vacío.");
			return;
		}
		
		
		
		
		if (!textFieldID.getText().equals("")) {// compruebo que sea un id ya existente para update

			if (JOptionPane.showConfirmDialog(this,"Desea sobreescribir el empleado con id: " + textFieldID.getText() + "?", "CUIDADO",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Empleado ejemplo = new Empleado();
				ejemplo.setNombre(textFieldNombre.getText().toUpperCase().trim());
				ejemplo.setDireccion(textFieldDireccion.getText());
				
				Empleado alExiste=AppMain.per.buscarEmpleado(ejemplo);//compruebo si existe uno con el mismo nombre
				if (alExiste!=null && alExiste.getId()!=Integer.parseInt(textFieldID.getText())) {
					JOptionPane.showMessageDialog(this, "No se pudo sobreescribir el empleado porque ya hay uno con el mismo nombre.");
					return;
				}
				ejemplo.setId(Integer.parseInt(textFieldID.getText()));
				alExiste=AppMain.per.buscarEmpleado(ejemplo);
				try {
					
					System.out.println("aaaaAAAAAAA"+alExiste.getId()+"AAAAAAAAAAAAAAAAA");
					alExiste.setNombre(textFieldNombre.getText().toUpperCase().trim());
					alExiste.setDireccion(textFieldDireccion.getText());
					
					
					if (textFieldFecha.getText().length()>0) {
						System.out.println(textFieldFecha.getText()+" "+ textFieldFecha.getText().length());
						DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
						 try {
							alExiste.setFechaNac(dateFormat.parse(textFieldFecha.getText().trim()));
						} catch (ParseException e) {
							JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy");
							e.printStackTrace();
							return;
						}
					}else {
						alExiste.setFechaNac(null);
					}
					
					AppMain.per.guardarEmpleado(alExiste);
					JOptionPane.showMessageDialog(this, "Empleado guardado correctamente.");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} else {// creo uno nuevo
			if (JOptionPane.showConfirmDialog(this,"Desea crear el empleado  " + textFieldNombre.getText().trim().toUpperCase() + "?", "CONFIRMAR",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Empleado ejemplo = new Empleado();
				ejemplo.setNombre(textFieldNombre.getText().toUpperCase().trim());
				ejemplo.setDireccion(textFieldDireccion.getText());
				
				System.out.println(textFieldFecha.getText().length()+"    fecha");
				if (textFieldFecha.getText().length()>0) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
					 try {
						ejemplo.setFechaNac(dateFormat.parse(textFieldFecha.getText().trim()));
					} catch (ParseException e) {
						JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: DD/MM/YYYY");
						e.printStackTrace();
						return;
					}
				}
				
				
				Empleado alExiste=AppMain.per.buscarEmpleado(ejemplo);//compruebo si existe uno con el mismo nombre
				if (alExiste!=null ) {
					JOptionPane.showMessageDialog(this, "No se pudo guardar el empleado porque ya hay uno con el mismo nombre.");
					return;
				}
				
			try {
				AppMain.per.guardarEmpleado(ejemplo);
				JOptionPane.showMessageDialog(this, "Empleado guardado correctamente.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
			
		}
		inicio();
	}

	protected void buscar() {
		
		textFieldFecha.setText("");
		textFieldID.setText("");
		textFieldNombre.setText(textFieldNombre.getText().trim().toUpperCase());
		

		 
			aniadirTabla();
			
			
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);
		
		
		
	}

	private void aniadirTabla() {
		alEmpleado = AppMain.per.extraerEmpleado(textFieldNombre.getText().toUpperCase().trim(), textFieldDireccion.getText());
		System.out.println(alEmpleado.size());
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alEmpleado.size(); i++) {
			Object[] f = new Object[5];
			f[0] = alEmpleado.get(i).getId();// añado id
			f[1] = alEmpleado.get(i).getNombre();// añado descripcion
			f[2] = alEmpleado.get(i).getDireccion();
			
			Date date = alEmpleado.get(i).getFechaNac(); 
			String strDate="";
			System.out.println();
			if (date!=null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
				 strDate = dateFormat.format(date);
				 System.out.println("date date : "+strDate);
			}
			System.out.println("date date : "+strDate);
			f[3] = strDate;
			model.addRow(f);// lo inserto en tabla
		}

	}

	protected void actualizarFormulario() {
		
		if (table.getSelectedRow() != -1) {
			
			Date date = alEmpleado.get(table.getSelectedRow()).getFechaNac(); 
			String strDate="";
			if (date!=null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
				 strDate = dateFormat.format(date);
			}
			
			textFieldNombre.setText(alEmpleado.get(table.getSelectedRow()).getNombre());
			textFieldID.setText(String.valueOf(alEmpleado.get(table.getSelectedRow()).getId()));
			textFieldFecha.setText(strDate);
			textFieldDireccion.setText(alEmpleado.get(table.getSelectedRow()).getDireccion());
			//ewfewfwf
			
			
				
		}

	}

	private void inicio() {
		
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		textFieldID.setText("");
		textFieldNombre.setText("");
		textFieldFecha.setText("");
		textFieldDireccion.setText("");
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		
		
		
	}
}
