package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Empleado;
import hibernate.Nomina;

import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class NominaNuevaGUI extends JFrame {
	static javax.swing.JFrame padre;
	Nomina nomina;
	static Empleado emp;
	

	private JPanel contentPane;
	private JButton btnSalir;
	private JButton btnGuardar;
	private JLabel lblFechaDeEmision;
	private JTextField textFieldImporte;
	private JTextField textFieldIRPF;
	private JTextField textFieldSS;
	private JTextField textFieldFecha;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NominaNuevaGUI frame = new NominaNuevaGUI();

					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param e 
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public NominaNuevaGUI() {
		this.padre=padre;
		setTitle("Nueva nomina");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 574, 315);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(238, 232, 170));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnGuardar = new JButton("Guardar nomina");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setBounds(86, 200, 134, 23);
		contentPane.add(btnGuardar);

		btnSalir = new JButton("Salir sin guardar");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salir();
			}
		});
		btnSalir.setBounds(230, 200, 121, 23);
		contentPane.add(btnSalir);

		btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(393, 88, 117, 55);
		contentPane.add(btnLimpiar);

		lblFechaDeEmision = new JLabel("Fecha de emision");
		lblFechaDeEmision.setBounds(90, 40, 109, 14);
		contentPane.add(lblFechaDeEmision);

		textFieldFecha = new JTextField();
		textFieldFecha.setToolTipText("dd/MM/yyyy");
		textFieldFecha.setBounds(228, 37, 123, 20);
		contentPane.add(textFieldFecha);
		textFieldFecha.setColumns(10);

		JLabel lblImporteBruto = new JLabel("Importe bruto");
		lblImporteBruto.setBounds(90, 81, 109, 14);
		contentPane.add(lblImporteBruto);

		textFieldImporte = new JTextField();
		textFieldImporte.setToolTipText("");
		textFieldImporte.setColumns(10);
		textFieldImporte.setBounds(228, 78, 123, 20);
		contentPane.add(textFieldImporte);

		textFieldIRPF = new JTextField();
		textFieldIRPF.setToolTipText("");
		textFieldIRPF.setColumns(10);
		textFieldIRPF.setBounds(228, 123, 123, 20);
		contentPane.add(textFieldIRPF);

		JLabel lblIfrp = new JLabel("IRPF");
		lblIfrp.setBounds(90, 129, 89, 14);
		contentPane.add(lblIfrp);

		textFieldSS = new JTextField();
		textFieldSS.setToolTipText("");
		textFieldSS.setColumns(10);
		textFieldSS.setBounds(228, 154, 123, 20);
		contentPane.add(textFieldSS);

		JLabel lblSeguridadSocial = new JLabel("Seguridad social");
		lblSeguridadSocial.setBounds(90, 160, 101, 14);
		contentPane.add(lblSeguridadSocial);

		////
		inicio();

	}

	// getters setters
	public void setNomina(Nomina n) {
		this.nomina = n;
	}

	public Nomina getNomina() {

		return this.nomina;
	}

	// metodos
	private void inicio() {
		nomina = new Nomina();
		textFieldFecha.setText("");
		textFieldImporte.setText("");
		textFieldIRPF.setText("");
		textFieldSS.setText("");

	}

	protected void salir() {
		if (JOptionPane.showConfirmDialog(this, "�Desea salir sin guardar?", "CONFIRMAR",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			this.dispose();
		}
	}

	protected void guardar() {
		if (JOptionPane.showConfirmDialog(this, "�Desea guardar la nueva nomina?", "CONFIRMAR",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
		
		// COMPRUEBO QUE FECHA NO VACIA
		if (textFieldFecha.getText().length() == 0) {
			JOptionPane.showMessageDialog(this, "El campo de fecha de emision no puede estar vacio.");
			return;
		}
		// OBTENGO FECHA
		Date fecha = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			fecha = dateFormat.parse(textFieldFecha.getText().trim());
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy");
			e.printStackTrace();
			return;
		}
		// COMPRUEBO QUE EL IMPORTE NO ESTE VACIO
		Double importe = 0.0;
		if (textFieldImporte.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "El importe bruto no puede estar vacio");
			return;
		}
		// COMPRUEBO QUE SEA UN DOUBLE EL IMPORTE
		try {
			importe = Double.valueOf(textFieldImporte.getText());
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(this, "El formato de importe debe ser: NUM.DEC");
			ex.printStackTrace();
			return;
		}
		

		// COMPRUEBO QUE EL IRPF NO ESTE VACIO
		Double irpf = 0.0;
		if (textFieldIRPF.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "El IRPF no puede estar vacio");
			return;
		}
		// COMPRUEBO QUE SEA UN DOUBLE EL IRPF
		try {
			irpf = Double.valueOf(textFieldImporte.getText());
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(this, "El formato de IRPF debe ser: NUM.DEC");
			ex.printStackTrace();
			return;
		}

		// COMPRUEBO QUE SEGURIDAD SOCIAL NO ESTE VACIO
		Double ss = 0.0;
		if (textFieldImporte.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Seguridad social no puede estar vacia");
			return;
		}
		// COMPRUEBO QUE SEA UN DOUBLE SEGURIDAD SOCIAL
		try {
			ss = Double.valueOf(textFieldImporte.getText());
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(this, "El formato de seguridad social debe ser: NUM.DEC");
			ex.printStackTrace();
			return;
		}
		
		//todo correcto ahora guardo
		if (nomina==null) {
			System.out.println("null");
		}
		nomina.setFechaEmision(fecha);
		nomina.setImporteBruto(importe);
		nomina.setIrpf(irpf);
		nomina.setSegSocial(ss);
		nomina.setEmpleado(emp);
		JOptionPane.showMessageDialog(this, "Nomina guardada correctamente.");
		
		dispose();
		}
	}

}
