package paqueteFinal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


//añadir implements persistencia
public class PersistenciaMySQL  {

	Connection cn; // Conexion con la BD

	public PersistenciaMySQL(String servidor, String puerto, String baseDatos, String usuario, String password) throws ClassNotFoundException, SQLException {

		String cadenaConexion = "";
		Class.forName("com.mysql.jdbc.Driver");
		cadenaConexion = "jdbc:mysql://" + servidor + ":" + puerto + "/" + baseDatos;
		cn = DriverManager.getConnection(cadenaConexion, usuario, password);

	}
}
