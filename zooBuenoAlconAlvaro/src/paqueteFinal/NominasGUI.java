package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Empleado;
import hibernate.Nomina;

import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class NominasGUI extends JFrame {
	static javax.swing.JFrame padre;
	List<Empleado> alEmpleado;
	List<Nomina> alNomina;

	private JPanel contentPane;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscar;
	private JTable table;
	private JScrollPane scrollPane;
	private JLabel lblFechaDeEmision;
	private JTextField textFieldImporte;
	private JTextField textFieldIRPF;
	private JTextField textFieldSS;
	private JComboBox comboBoxEmpleado;
	private JTextField textFieldFecha;
	private JButton btnLimpiar;
	private JButton btnNuevaNomina;

	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NominasGUI frame = new NominasGUI(padre);

					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param appMain 
	 */
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
            padre.setEnabled(true);
            padre.setVisible(true);
        }
    }
	
	public NominasGUI(javax.swing.JFrame padre) {
		this.padre=padre;
		setTitle("Mantenimiento de NOMINAS");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 840, 462);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(238, 232, 170));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnBuscar = new JButton("Buscar nominas");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscar();
			}
		});
		btnBuscar.setBounds(267, 26, 141, 23);
		contentPane.add(btnBuscar);

		btnGuardar = new JButton("Guardar nomina");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(21, 226, 134, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar nomina");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(267, 226, 121, 23);
		contentPane.add(btnBorrar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 285, 804, 127);
		contentPane.add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try {
					actualizarFormulario();
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		scrollPane.setViewportView(table);

		ListSelectionModel listSelectionModel = table.getSelectionModel();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		table.setDefaultEditor(Object.class, null);

		btnLimpiar = new JButton("Limpiar formulario");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(648, 10, 149, 55);
		contentPane.add(btnLimpiar);

		JLabel lblEmpleado = new JLabel("EMPLEADO");
		lblEmpleado.setBounds(35, 30, 73, 14);
		contentPane.add(lblEmpleado);

		comboBoxEmpleado = new JComboBox();
		comboBoxEmpleado.setBounds(108, 27, 123, 20);
		contentPane.add(comboBoxEmpleado);

		lblFechaDeEmision = new JLabel("Fecha de emision");
		lblFechaDeEmision.setBounds(35, 102, 109, 14);
		contentPane.add(lblFechaDeEmision);

		textFieldFecha = new JTextField();
		textFieldFecha.setToolTipText("dd/MM/yyyy");
		textFieldFecha.setBounds(154, 102, 123, 20);
		contentPane.add(textFieldFecha);
		textFieldFecha.setColumns(10);

		JLabel lblImporteBruto = new JLabel("Importe bruto");
		lblImporteBruto.setBounds(35, 145, 109, 14);
		contentPane.add(lblImporteBruto);

		textFieldImporte = new JTextField();
		textFieldImporte.setToolTipText("");
		textFieldImporte.setColumns(10);
		textFieldImporte.setBounds(154, 145, 123, 20);
		contentPane.add(textFieldImporte);

		textFieldIRPF = new JTextField();
		textFieldIRPF.setToolTipText("");
		textFieldIRPF.setColumns(10);
		textFieldIRPF.setBounds(398, 105, 123, 20);
		contentPane.add(textFieldIRPF);

		JLabel lblIfrp = new JLabel("IRPF");
		lblIfrp.setBounds(287, 105, 89, 14);
		contentPane.add(lblIfrp);

		textFieldSS = new JTextField();
		textFieldSS.setToolTipText("");
		textFieldSS.setColumns(10);
		textFieldSS.setBounds(398, 148, 123, 20);
		contentPane.add(textFieldSS);

		JLabel lblSeguridadSocial = new JLabel("Seguridad social");
		lblSeguridadSocial.setBounds(287, 151, 101, 14);
		contentPane.add(lblSeguridadSocial);

		btnNuevaNomina = new JButton("Nueva nomina");
		btnNuevaNomina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nuevaNomina();
			}
		});
		btnNuevaNomina.setEnabled(false);
		btnNuevaNomina.setBounds(648, 87, 149, 55);
		contentPane.add(btnNuevaNomina);

		////

		model.addColumn("ID");
		model.addColumn("FECHA EMISION");
		model.addColumn("I.BRUTO");
		model.addColumn("% IRPF");
		model.addColumn("% SEG. SOCIAL");
		table.setFillsViewportHeight(true);
		inicio();

		table.setFillsViewportHeight(true);
	}

	protected void actualizarFormulario() {

		if (table.getSelectedRow() != -1) {

			Date date = alNomina.get(table.getSelectedRow()).getFechaEmision();
			String strDate = "";
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				strDate = dateFormat.format(date);
			}

			textFieldImporte.setText(String.valueOf(alNomina.get(table.getSelectedRow()).getImporteBruto()));

			textFieldIRPF.setText(String.valueOf(alNomina.get(table.getSelectedRow()).getIrpf()));
			textFieldFecha.setText(strDate);
			textFieldSS.setText(String.valueOf(alNomina.get(table.getSelectedRow()).getSegSocial()));
			
			btnBorrar.setEnabled(true);
			btnGuardar.setEnabled(true);
		}

	}

	private void inicio() {
		alEmpleado = AppMain.per.extraerEmpleado("", "");
		System.out.println(alEmpleado.size());
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		btnNuevaNomina.setEnabled(false);
		comboBoxEmpleado.setEnabled(true);

		textFieldFecha.setText("");
		textFieldImporte.setText("");
		textFieldIRPF.setText("");
		textFieldSS.setText("");

		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alEmpleado.size(); i++) {
			System.out.println(alEmpleado.get(i).getNombre());
			comboBoxEmpleado.addItem(alEmpleado.get(i).getNombre());
		}

	}

	protected void nuevaNomina() {
		System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEEEEEE");

		Empleado e = new Empleado();
		e.setId((alEmpleado.get(comboBoxEmpleado.getSelectedIndex()).getId()));
		e = AppMain.per.buscarEmpleado(e);

		Nomina nomina = new Nomina();

		if (JOptionPane.showConfirmDialog(this, "�Desea guardar la nueva nomina?", "CONFIRMAR",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

			// COMPRUEBO QUE FECHA NO VACIA
			if (textFieldFecha.getText().length() == 0) {
				JOptionPane.showMessageDialog(this, "El campo de fecha de emision no puede estar vacio.");
				return;
			}
			// OBTENGO FECHA
			Date fecha = new Date();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			try {
				fecha = dateFormat.parse(textFieldFecha.getText().trim());
			} catch (ParseException ee) {
				JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy");
				ee.printStackTrace();
				return;
			}
			// COMPRUEBO QUE EL IMPORTE NO ESTE VACIO
			Double importe = 0.0;
			if (textFieldImporte.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "El importe bruto no puede estar vacio");
				return;
			}
			// COMPRUEBO QUE SEA UN DOUBLE EL IMPORTE
			try {
				importe = Double.valueOf(textFieldImporte.getText());
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "El formato de importe debe ser: NUM.DEC");
				ex.printStackTrace();
				return;
			}

			// COMPRUEBO QUE EL IRPF NO ESTE VACIO
			Double irpf = 0.0;
			if (textFieldIRPF.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "El IRPF no puede estar vacio");
				return;
			}
			// COMPRUEBO QUE SEA UN DOUBLE EL IRPF
			try {
				irpf = Double.valueOf(textFieldIRPF.getText());
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "El formato de IRPF debe ser: NUM.DEC");
				ex.printStackTrace();
				return;
			}

			// COMPRUEBO QUE SEGURIDAD SOCIAL NO ESTE VACIO
			Double ss = 0.0;
			if (textFieldSS.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "Seguridad social no puede estar vacia");
				return;
			}
			// COMPRUEBO QUE SEA UN DOUBLE SEGURIDAD SOCIAL
			try {
				ss = Double.valueOf(textFieldSS.getText());
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "El formato de seguridad social debe ser: NUM.DEC");
				ex.printStackTrace();
				return;
			}
			
			//compruebo que tienen valores correctos IRPF y SS
			if (0>irpf || irpf>100 || 0>ss || ss>100) {
				JOptionPane.showMessageDialog(this, "El valor de los campos IRPF Y Seguridad Social debe estar entre 0 y 100.");
				return;
			}
			
			if (irpf+ss>100) {
				JOptionPane.showMessageDialog(this, "La suma de IRPF y Seguridad Social no puede superar 100.");
				return;
			}

			// todo correcto ahora guardo
			if (nomina == null) {
				System.out.println("null");
			}
			nomina.setFechaEmision(fecha);
			nomina.setImporteBruto(importe);
			nomina.setIrpf(irpf);
			nomina.setSegSocial(ss);
			nomina.setEmpleado(e);
			e.getNominas().add(nomina);
			try {
				AppMain.per.guardarEmpleado(e);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(this, "Nomina guardada correctamente.");

		}

		System.out.println("aaaaaaaaaaaaaaaaaaa");
		actualizarNominas();

	}

	private void actualizarNominas() {
		System.out.println("actualizo");
		alNomina = AppMain.per
				.extraerNomina(String.valueOf(alEmpleado.get(comboBoxEmpleado.getSelectedIndex()).getId()));

		System.out.println(alNomina.size() + "alnomina -actualzar");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);
		for (int i = 0; i < alNomina.size(); i++) {
			Object[] f = new Object[5];
			f[0] = alNomina.get(i).getId();// añado id

			Date date = alNomina.get(i).getFechaEmision();
			String strDate = "";
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				strDate = dateFormat.format(date);
			}
			f[1] = strDate;

			f[2] = alNomina.get(i).getImporteBruto();// añado descripcion
			f[3] = alNomina.get(i).getIrpf();
			f[4] = alNomina.get(i).getSegSocial();

			model.addRow(f);// lo inserto en tabla
		}

	}

	protected void borrar() {

		if (JOptionPane.showConfirmDialog(this, "�Desea borrar la nomina?", "CONFIRMAR",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

			Nomina n = new Nomina();
			n.setId((alNomina.get(table.getSelectedRow()).getId()));
			n = AppMain.per.buscarNomina(n);

			Empleado e = new Empleado();
			e.setId((alEmpleado.get(comboBoxEmpleado.getSelectedIndex()).getId()));
			e = AppMain.per.buscarEmpleado(e);

			System.out.println(e.getNominas().size() + "primero");
			System.out.println(e.getNominas().contains(n));
			System.out.println("id empl: " + e.getId() + "   id nom: " + n.getId());
			e.getNominas().remove(n);

			try {
				AppMain.per.guardarEmpleado(e);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			System.out.println(e.getNominas().size() + "segundo");
			AppMain.per.borrarNomina(n);

			JOptionPane.showMessageDialog(this, "Nomina borrada correctamente.");
			actualizarNominas();
			btnBorrar.setEnabled(false);
			btnGuardar.setEnabled(false);

			textFieldFecha.setText("");
			textFieldImporte.setText("");
			textFieldIRPF.setText("");
			textFieldSS.setText("");
		}
	}

	protected void guardar() {

		Empleado e = new Empleado();
		e.setId((alEmpleado.get(table.getSelectedRow()).getId()));
		e = AppMain.per.buscarEmpleado(e);

		Nomina nomina = new Nomina();

		if (JOptionPane.showConfirmDialog(this, "�Desea guardar la nomina?", "CONFIRMAR",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

			// COMPRUEBO QUE FECHA NO VACIA
			if (textFieldFecha.getText().length() == 0) {
				JOptionPane.showMessageDialog(this, "El campo de fecha de emision no puede estar vacio.");
				return;
			}
			// OBTENGO FECHA
			Date fecha = new Date();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			try {
				fecha = dateFormat.parse(textFieldFecha.getText().trim());
			} catch (ParseException ee) {
				JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy");
				ee.printStackTrace();
				return;
			}
			// COMPRUEBO QUE EL IMPORTE NO ESTE VACIO
			Double importe = 0.0;
			if (textFieldImporte.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "El importe bruto no puede estar vacio");
				return;
			}
			// COMPRUEBO QUE SEA UN DOUBLE EL IMPORTE
			try {
				importe = Double.valueOf(textFieldImporte.getText());
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "El formato de importe debe ser: NUM.DEC");
				ex.printStackTrace();
				return;
			}

			// COMPRUEBO QUE EL IRPF NO ESTE VACIO
			Double irpf = 0.0;
			if (textFieldIRPF.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "El IRPF no puede estar vacio");
				return;
			}
			// COMPRUEBO QUE SEA UN DOUBLE EL IRPF
			try {
				irpf = Double.valueOf(textFieldIRPF.getText());
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "El formato de IRPF debe ser: NUM.DEC");
				ex.printStackTrace();
				return;
			}

			// COMPRUEBO QUE SEGURIDAD SOCIAL NO ESTE VACIO
			Double ss = 0.0;
			if (textFieldSS.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "Seguridad social no puede estar vacia");
				return;
			}
			// COMPRUEBO QUE SEA UN DOUBLE SEGURIDAD SOCIAL
			try {
				ss = Double.valueOf(textFieldSS.getText());
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "El formato de seguridad social debe ser: NUM.DEC");
				ex.printStackTrace();
				return;
			}
			
			//compruebo que tienen valores correctos IRPF y SS
			if (0>irpf || irpf>100 || 0>ss || ss>100) {
				JOptionPane.showMessageDialog(this, "El valor de los campos IRPF Y Seguridad Social debe estar entre 0 y 100.");
				return;
			}
			
			if (irpf+ss>100) {
				JOptionPane.showMessageDialog(this, "La suma de IRPF y Seguridad Social no puede superar 100.");
				return;
			}
			
			

			// todo correcto ahora guardo
			if (nomina == null) {
				System.out.println("null");
			}
			// aniado id para buscar la nomina
			nomina.setId(alNomina.get(table.getSelectedRow()).getId());
			nomina = AppMain.per.buscarNomina(nomina);
			// modifico los datos
			nomina.setFechaEmision(fecha);
			nomina.setImporteBruto(importe);
			nomina.setIrpf(irpf);
			nomina.setSegSocial(ss);
			System.out.println(nomina.getEmpleado().getNombre() + "empleado");
			// 12/12/12
			try {
				System.out.println(nomina.getEmpleado().getNombre() + "empleado2");
				AppMain.per.guardarNomina(nomina);
				AppMain.per.guardarEmpleado(e);
				System.out.println(e.getNominas().size() + "nominas size");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(this, "Nomina guardada correctamente.");

		

		actualizarNominas();

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);

		textFieldFecha.setText("");
		textFieldImporte.setText("");
		textFieldIRPF.setText("");
		textFieldSS.setText("");
		}
	}

	protected void buscar() {
		actualizarNominas();
		comboBoxEmpleado.setEnabled(false);
		btnNuevaNomina.setEnabled(true);
	}

	private void aniadirTabla() {

		System.out.println(alEmpleado.size() + "alempleado size");
		DefaultTableModel model = (DefaultTableModel) table.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alEmpleado.size(); i++) {
			Object[] f = new Object[5];
			f[0] = alEmpleado.get(i).getId();// añado id
			f[1] = alEmpleado.get(i).getNombre();// añado descripcion
			f[2] = alEmpleado.get(i).getDireccion();

			Date date = alEmpleado.get(i).getFechaNac();
			String strDate = "";
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				strDate = dateFormat.format(date);
				System.out.println("date date : " + strDate);
			}
			System.out.println("date date : " + strDate);
			f[3] = strDate;
			model.addRow(f);// lo inserto en tabla
		}

	}

}
