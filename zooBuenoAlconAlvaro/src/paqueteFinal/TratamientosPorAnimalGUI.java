package paqueteFinal;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import hibernate.Alimento;
import hibernate.Animal;
import hibernate.Animaltratamiento;
import hibernate.AnimaltratamientoId;
import hibernate.Empleado;
import hibernate.Especie;
import hibernate.Tratamiento;
import hibernate.Zona;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class TratamientosPorAnimalGUI extends JFrame {
	static javax.swing.JFrame padre;
	Animal animal;
	boolean animalSeleccionado;
	Tratamiento tratamiento;
	boolean tratamientoSeleccionado;
	Empleado empleado;
	boolean empleadoSeleccionado;

	List<Animal> alAnimales;
	List<Especie> lEspecie;
	List<Tratamiento> lTratamiento;
	List<Empleado> lEmpleado;
	ArrayList<Animaltratamiento> lAnimaltratamiento;
	ArrayList<Animaltratamiento> lAnimaltratamientoBorrar;

	private JPanel contentPane;
	private JTextField textFieldID;
	private JTextField textFieldNombre;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnBuscarAnimal;
	private JTable tableAnimal;
	private JComboBox comboBoxEspecie;
	private JScrollPane scrollPane;
	private JTable tableTratamientos;
	private JTextField textFieldAlimentoGuardar;
	private JTextField textFieldFecha;
	private JLabel lblTratamientoInv;
	private JLabel lblFechaInv;
	private JComboBox comboBoxTratamiento;
	private JButton btnSeleccionarTratamiento;
	private JButton buttonBuscarTratamientos;
	private JButton btnSeleccionarAnimal;
	private JComboBox comboBoxEmpleado;
	private JButton btnSeleccionarEmpleado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TratamientosPorAnimalGUI frame = new TratamientosPorAnimalGUI(padre);

					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			padre.setEnabled(true);
			padre.setVisible(true);
		}
	}

	public TratamientosPorAnimalGUI(javax.swing.JFrame padre) {
		this.padre = padre;
		setTitle("Tratamientos a animal");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1016, 771);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldID = new JTextField();
		textFieldID.setEnabled(false);
		textFieldID.setBounds(83, 31, 86, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(10, 34, 21, 14);
		contentPane.add(lblId);

		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(83, 62, 86, 20);
		contentPane.add(textFieldNombre);

		JLabel lblZona = new JLabel("NOMBRE");
		lblZona.setBounds(10, 65, 54, 14);
		contentPane.add(lblZona);

		btnBuscarAnimal = new JButton("Buscar animal");
		btnBuscarAnimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscarAnimal();
			}
		});
		btnBuscarAnimal.setBounds(10, 121, 159, 32);
		contentPane.add(btnBuscarAnimal);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();

			}
		});
		btnGuardar.setBounds(879, 687, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(10, 552, 159, 23);
		contentPane.add(btnBorrar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(189, 33, 779, 163);
		contentPane.add(scrollPane);

		tableAnimal = new JTable();
		tableAnimal.addMouseListener(new MouseAdapter() {

		});
		scrollPane.setViewportView(tableAnimal);

		ListSelectionModel listSelectionModel = tableAnimal.getSelectionModel();
		tableAnimal.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		tableAnimal.setDefaultEditor(Object.class, null);

		JButton btnLimpiar = new JButton("INICIO");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inicio();
			}
		});
		btnLimpiar.setBounds(189, 675, 140, 46);
		contentPane.add(btnLimpiar);

		JLabel lblCoste = new JLabel("ESPECIE");
		lblCoste.setBounds(10, 96, 73, 14);
		contentPane.add(lblCoste);

		comboBoxEspecie = new JComboBox();
		comboBoxEspecie.setBounds(83, 93, 86, 20);
		contentPane.add(comboBoxEspecie);

		JLabel lblAnimal = new JLabel("ANIMAL");
		lblAnimal.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAnimal.setBounds(523, 8, 126, 14);
		contentPane.add(lblAnimal);

		btnSeleccionarAnimal = new JButton("Seleccionar animal");
		btnSeleccionarAnimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarAnimal();
			}
		});
		btnSeleccionarAnimal.setBounds(10, 164, 159, 32);
		contentPane.add(btnSeleccionarAnimal);

		btnSeleccionarTratamiento = new JButton("Seleccionar tratamiento");
		btnSeleccionarTratamiento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarTratamiento();
			}
		});
		btnSeleccionarTratamiento.setBounds(279, 253, 203, 32);
		contentPane.add(btnSeleccionarTratamiento);

		buttonBuscarTratamientos = new JButton("Buscar");
		buttonBuscarTratamientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarConsumos();
			}
		});
		buttonBuscarTratamientos.setFont(new Font("Tahoma", Font.BOLD, 12));
		buttonBuscarTratamientos.setBounds(10, 465, 159, 65);
		contentPane.add(buttonBuscarTratamientos);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(189, 465, 779, 163);
		contentPane.add(scrollPane_2);

		tableTratamientos = new JTable();
		tableTratamientos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				btnBorrar.setEnabled(true);
			}
		});
		tableTratamientos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableTratamientos.setFillsViewportHeight(true);
		scrollPane_2.setViewportView(tableTratamientos);

		textFieldAlimentoGuardar = new JTextField();
		textFieldAlimentoGuardar.setEnabled(false);
		textFieldAlimentoGuardar.setText("");
		textFieldAlimentoGuardar.setColumns(10);
		textFieldAlimentoGuardar.setBounds(659, 688, 86, 20);
		contentPane.add(textFieldAlimentoGuardar);

		textFieldFecha = new JTextField();
		textFieldFecha.setToolTipText("dd/MM/yyyy HH:mm");
		textFieldFecha.setEnabled(false);
		textFieldFecha.setText("");
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(769, 688, 86, 20);
		contentPane.add(textFieldFecha);

		lblTratamientoInv = new JLabel("TRATAMIENTO");
		lblTratamientoInv.setBounds(659, 664, 86, 14);
		contentPane.add(lblTratamientoInv);

		lblFechaInv = new JLabel("FECHA");
		lblFechaInv.setBounds(769, 664, 54, 14);
		contentPane.add(lblFechaInv);

		comboBoxTratamiento = new JComboBox();
		comboBoxTratamiento.setBounds(492, 253, 190, 32);
		contentPane.add(comboBoxTratamiento);

		JLabel lblTratamiento = new JLabel("TRATAMIENTO");
		lblTratamiento.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTratamiento.setBounds(500, 228, 147, 14);
		contentPane.add(lblTratamiento);

		JLabel lblTratamientos = new JLabel("TRATAMIENTOS ASOCIADOS");
		lblTratamientos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTratamientos.setBounds(426, 439, 277, 14);
		contentPane.add(lblTratamientos);

		JButton buttonConfirmar = new JButton("CONFIRMAR");
		buttonConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				confirmar();
			}
		});
		buttonConfirmar.setBounds(10, 675, 140, 46);
		contentPane.add(buttonConfirmar);

		btnSeleccionarEmpleado = new JButton("Seleccionar empleado");
		btnSeleccionarEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarEmpleado();
			}
		});
		btnSeleccionarEmpleado.setEnabled(true);
		btnSeleccionarEmpleado.setBounds(279, 347, 203, 32);
		contentPane.add(btnSeleccionarEmpleado);

		comboBoxEmpleado = new JComboBox();
		comboBoxEmpleado.setEnabled(true);
		comboBoxEmpleado.setBounds(492, 347, 190, 32);
		contentPane.add(comboBoxEmpleado);

		JLabel lblEmpleado = new JLabel("EMPLEADO");
		lblEmpleado.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEmpleado.setBounds(500, 322, 147, 14);
		contentPane.add(lblEmpleado);
		////
		DefaultTableModel model = (DefaultTableModel) tableAnimal.getModel();

		model.addColumn("ID");
		model.addColumn("Nombre");
		model.addColumn("Especie");
		model.addColumn("Zona");
		model.addColumn("FechaNac");
		tableAnimal.setFillsViewportHeight(true);

		DefaultTableModel model2 = (DefaultTableModel) tableTratamientos.getModel();
		model2.addColumn("Animal");
		model2.addColumn("Tratamiento");
		model2.addColumn("Empleado");
		model2.addColumn("fecha");
		tableTratamientos.setFillsViewportHeight(true);

		//
		comboBoxEspecie.removeAllItems();

		lEspecie = AppMain.per.extraerEspecie("");
		for (int i = 0; i < lEspecie.size(); i++) {
			comboBoxEspecie.addItem(lEspecie.get(i).getDescripcion());
		}

		lTratamiento = AppMain.per.extraerTratamiento("");
		for (int i = 0; i < lTratamiento.size(); i++) {
			comboBoxTratamiento.addItem(lTratamiento.get(i).getDescripcion());
		}

		lEmpleado = AppMain.per.extraerEmpleado("", "");
		for (int i = 0; i < lEmpleado.size(); i++) {
			comboBoxEmpleado.addItem(lEmpleado.get(i).getNombre());
		}
		//
		inicio();
	}

	protected void confirmar() {

		if (JOptionPane.showConfirmDialog(this, "�Estas seguro de confirmar?", "CUIDADO",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

			for (Animaltratamiento animtrat : lAnimaltratamientoBorrar) {
				Animal a = new Animal();
				a.setId(animtrat.getAnimal().getId());
				a = AppMain.per.buscarAnimal(a);

				Tratamiento t = new Tratamiento();
				t.setId(animtrat.getTratamiento().getId());
				t = AppMain.per.buscarTratamiento(t);

				Empleado e = new Empleado();
				e.setId(animtrat.getEmpleado().getId());
				e = AppMain.per.buscarEmpleado(e);

				if (a.getAnimaltratamientos().contains(animtrat)) {
					a.getAnimaltratamientos().remove(animtrat);
					System.out.println("borrado de animal" + a.getId());
				}
				if (t.getAnimaltratamientos().contains(animtrat)) {
					t.getAnimaltratamientos().remove(animtrat);
					System.out.println("borrado de tratamiento" + t.getId());
				}
				if (e.getAnimaltratamientos().contains(animtrat)) {
					e.getAnimaltratamientos().remove(animtrat);
					System.out.println("borrado de empleado" + e.getId());
				}
				AppMain.per.borrarSinCommit(animtrat);
				AppMain.per.transaccionCommit();
				try {
					AppMain.per.guardarEmpleado(e);
					AppMain.per.guardarTratamiento(t);
					AppMain.per.guardarAnimal(a);
				} catch (SQLException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				} catch (ParseException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				}

			}

			for (Animaltratamiento animtrat : lAnimaltratamiento) {
				Animal a = new Animal();
				a.setId(animtrat.getAnimal().getId());
				a = AppMain.per.buscarAnimal(a);

				Tratamiento t = new Tratamiento();
				t.setId(animtrat.getTratamiento().getId());
				t = AppMain.per.buscarTratamiento(t);

				Empleado e = new Empleado();
				e.setId(animtrat.getEmpleado().getId());
				e = AppMain.per.buscarEmpleado(e);

				if (!a.getAnimaltratamientos().contains(animtrat)) {
					a.getAnimaltratamientos().add(animtrat);
				}
				if (!empleado.getAnimaltratamientos().contains(animtrat)) {
					empleado.getAnimaltratamientos().add(animtrat);
				}
				if (!tratamiento.getAnimaltratamientos().contains(animtrat)) {
					tratamiento.getAnimaltratamientos().add(animtrat);
				}
				
				AppMain.per.guardarSinCommit(animtrat);
				AppMain.per.transaccionCommit();

				try {
					AppMain.per.guardarEmpleado(e);
					AppMain.per.guardarTratamiento(t);
					AppMain.per.guardarAnimal(a);
					// AppMain.per.guardarTratamiento(t);
				} catch (SQLException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				} catch (ParseException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				}

			}

			lAnimaltratamiento.removeAll(lAnimaltratamientoBorrar);
//			animal.getAnimaltratamientos().clear();
//			animal.getAnimaltratamientos().addAll(lAnimaltratamiento);

			JOptionPane.showMessageDialog(this, "Guardado correctamente.");
			inicio();
		}

	}

	protected void buscarConsumos() {
		System.out.println(animalSeleccionado);
		System.out.println(tratamientoSeleccionado);
		System.out.println(empleadoSeleccionado);
		if (animalSeleccionado == false && tratamientoSeleccionado == false && empleadoSeleccionado == false) {
			JOptionPane.showMessageDialog(this, "Debes seleccionar al menos un animal y/o tratamiento y/o empleado.");
			return;
		}

		// animal
		if (animalSeleccionado == true && tratamientoSeleccionado == false && empleadoSeleccionado == false) {

			lAnimaltratamiento = new ArrayList<>(animal.getAnimaltratamientos());

			if (lAnimaltratamiento.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay tratamientos asociados disponibles.");
				return;
			}
		}
		// tratamiento
		if (animalSeleccionado == false && tratamientoSeleccionado == true && empleadoSeleccionado == false) {
			System.out.println("solo tratamiento");
			lAnimaltratamiento = new ArrayList<>(tratamiento.getAnimaltratamientos());
			if (lAnimaltratamiento.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay tratamientos asociados disponibles.");
				return;
			}
		}
		// empleado
		if (animalSeleccionado == false && tratamientoSeleccionado == false && empleadoSeleccionado == true) {
			lAnimaltratamiento = new ArrayList<>(empleado.getAnimaltratamientos());
			if (lAnimaltratamiento.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay tratamientos asociados disponibles.");
				return;
			}
		}
		// animal-tratamiento
		if (animalSeleccionado == true && tratamientoSeleccionado == true && empleadoSeleccionado == false) {
			lAnimaltratamiento = new ArrayList<>(animal.getAnimaltratamientos());

			ArrayList lAnimaltratamientoborrar = new ArrayList<>();
			for (int i = 0; i < lAnimaltratamiento.size(); i++) {
				if (!lAnimaltratamiento.get(i).getTratamiento().getDescripcion().equals(tratamiento.getDescripcion())) {
					lAnimaltratamientoborrar.add(lAnimaltratamiento.get(i));
				}
			}
			lAnimaltratamiento.removeAll(lAnimaltratamientoborrar);

			// for (Animaltratamiento animalt : lAnimaltratamiento) {
			// System.out.println("animaltrat:"+animalt.getTratamiento().getId()+" trat
			// id:"+tratamiento.getId());
			// if (animalt.getTratamiento().getId()!=(tratamiento.getId())) {
			// lAnimaltratamiento.remove(animalt);
			// }
			// }

			if (lAnimaltratamiento.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay tratamientos asociados disponibles.");
				return;
			}
		}
		// animal-empleado
		if (animalSeleccionado == true && tratamientoSeleccionado == false && empleadoSeleccionado == true) {
			lAnimaltratamiento = new ArrayList<>(animal.getAnimaltratamientos());
			// for (int i = 0; i < lAnimaltratamiento.size(); i++) {
			// if
			// (!lAnimaltratamiento.get(i).getEmpleado().getId().equals(empleado.getId())) {
			// lAnimaltratamiento.remove(i);
			// }
			// }

			ArrayList lAnimaltratamientoborrar = new ArrayList<>();
			for (int i = 0; i < lAnimaltratamiento.size(); i++) {
				if (!lAnimaltratamiento.get(i).getEmpleado().getNombre().equals(empleado.getNombre())) {
					lAnimaltratamientoborrar.add(lAnimaltratamiento.get(i));
				}
			}
			lAnimaltratamiento.removeAll(lAnimaltratamientoborrar);

			if (lAnimaltratamiento.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay tratamientos asociados disponibles.");
				return;
			}
		}

		// tratamiento-empleado
		if (animalSeleccionado == false && tratamientoSeleccionado == true && empleadoSeleccionado == true) {
			lAnimaltratamiento = new ArrayList<>(tratamiento.getAnimaltratamientos());
			// for (int i = 0; i < lAnimaltratamiento.size(); i++) {
			// if
			// (!lAnimaltratamiento.get(i).getEmpleado().getId().equals(empleado.getId())) {
			// lAnimaltratamiento.remove(i);
			// }
			// }
			ArrayList lAnimaltratamientoborrar = new ArrayList<>();
			for (int i = 0; i < lAnimaltratamiento.size(); i++) {
				if (!lAnimaltratamiento.get(i).getEmpleado().getNombre().equals(empleado.getNombre())) {
					lAnimaltratamientoborrar.add(lAnimaltratamiento.get(i));
				}
			}
			lAnimaltratamiento.removeAll(lAnimaltratamientoborrar);

			if (lAnimaltratamiento.size() == 0) {
				JOptionPane.showMessageDialog(this, "No hay tratamientos asociados disponibles.");
				return;
			}
		}
		// animal-tratamiento-empleado
		if (animalSeleccionado == true && tratamientoSeleccionado == true && empleadoSeleccionado == true) {
			lAnimaltratamiento = new ArrayList<>(animal.getAnimaltratamientos());
			// for (int i = 0; i < lAnimaltratamiento.size(); i++) {
			// if
			// (!lAnimaltratamiento.get(i).getEmpleado().getId().equals(empleado.getId())) {
			// lAnimaltratamiento.remove(i);
			// }
			// if(!lAnimaltratamiento.get(i).getTratamiento().getId().equals(tratamiento.getId()))
			// {
			// lAnimaltratamiento.remove(i);
			// }
			// }
			ArrayList lAnimaltratamientoborrar = new ArrayList<>();
			for (int i = 0; i < lAnimaltratamiento.size(); i++) {
				if (!lAnimaltratamiento.get(i).getEmpleado().getNombre().equals(empleado.getNombre())) {
					lAnimaltratamientoborrar.add(lAnimaltratamiento.get(i));
				}
			}
			for (int i = 0; i < lAnimaltratamiento.size(); i++) {
				if (!lAnimaltratamiento.get(i).getTratamiento().getDescripcion().equals(tratamiento.getDescripcion())) {
					lAnimaltratamientoborrar.add(lAnimaltratamiento.get(i));
				}
			}
			lAnimaltratamiento.removeAll(lAnimaltratamientoborrar);

			if (lAnimaltratamiento.size() == 0) {
				crearNuevoTratamientoAnimalEmpleado();
			}
		}

		if (lAnimaltratamiento == null) {
			JOptionPane.showMessageDialog(this, "Debes seleccionar al menos un animal y/o tratamiento y/o empleado.");
			return;
		}

		if (lAnimaltratamiento.size() > 0) {
			actualizarTablaTratamientos();
		}

		///////////////////////////////
		//
		// // si he seleccionado los dos
		// if (animalSeleccionado == true && alimentoSeleccionado == true) {
		// lConsume = new ArrayList<>(animal.getConsumes());
		//
		// for (int i = 0; i < lConsume.size(); i++) {
		// if
		// (!lConsume.get(i).getAlimento().getDescripcion().equals(alimento.getDescripcion()))
		// {
		// lConsume.remove(i);
		// }
		// }
		//
		// if (lConsume.size() == 0) {
		// crearNuevoTratamientoAnimalEmpleado();
		// }
		//
		// }
		//
		// // si animal solo seleccionado
		// if (animalSeleccionado == true && alimentoSeleccionado == false) {
		// lConsume = new ArrayList<>(animal.getConsumes());
		// if (lConsume.size() == 0) {
		// JOptionPane.showMessageDialog(this, "No hay consumos disponibles.");
		// return;
		// }
		//
		// }
		//
		// // si extraerConsumosConAlimento select de consume
		// if (animalSeleccionado == false && alimentoSeleccionado == true) {
		// lConsume = new ArrayList<>(alimento.getConsumes());
		// if (lConsume.size() == 0) {
		// JOptionPane.showMessageDialog(this, "No hay consumos disponibles.");
		// return;
		// }
		//
		// }
		//
		// if (lConsume == null) {
		// JOptionPane.showMessageDialog(this, "Debes seleccionar al menos animal o
		// alimento.");
		// return;
		// }
		//
		// if (lConsume.size() > 0) {
		// actualizarTablaTratamientos();
		// }
	}

	private void actualizarTablaTratamientos() {

		DefaultTableModel model = (DefaultTableModel) tableTratamientos.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < lAnimaltratamiento.size(); i++) {
			Object[] f = new Object[4];
			f[0] = lAnimaltratamiento.get(i).getAnimal().getNombre();// añado descripcion
			f[1] = lAnimaltratamiento.get(i).getTratamiento().getDescripcion();
			f[2] = lAnimaltratamiento.get(i).getEmpleado().getNombre();

			Date date = lAnimaltratamiento.get(i).getId().getFechaHora();
			String strDate = "";
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				strDate = dateFormat.format(date);
			}
			f[3] = strDate;

			model.addRow(f);// lo inserto en tabla
		}

	}

	private void crearNuevoTratamientoAnimalEmpleado() {
		if (JOptionPane.showConfirmDialog(this,
				"No existen tratamientos asociados con esta busqueda. �Desea guardar un nuevo tratamiento asociado?",
				"CUIDADO", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			textFieldAlimentoGuardar.setVisible(true);
			textFieldFecha.setVisible(true);
			lblTratamientoInv.setVisible(true);
			lblFechaInv.setVisible(true);
			btnGuardar.setVisible(true);
			textFieldFecha.setEnabled(true);
			btnGuardar.setEnabled(true);
			textFieldAlimentoGuardar.setText(tratamiento.getDescripcion());
		}

	}

	protected void seleccionarTratamiento() {
		if (lTratamiento == null) {
			System.out.println("No hay tratamientos disponibles");
			return;
		}

		// tratamiento.setId(lTratamiento.get(comboBoxTratamiento.getSelectedIndex()).getId());
		// tratamiento = AppMain.per.buscarTratamiento(tratamiento);
		tratamiento = lTratamiento.get(comboBoxTratamiento.getSelectedIndex());

		btnSeleccionarTratamiento.setEnabled(false);
		comboBoxTratamiento.setEnabled(false);
		tratamientoSeleccionado = true;
		AppMain.per.transaccionRefresh(tratamiento);
		DefaultTableModel model2 = (DefaultTableModel) tableTratamientos.getModel();// borro tabla
		System.out.println("tratamiento id " + tratamiento.getId());
		model2.setRowCount(0);
	}

	protected void seleccionarEmpleado() {
		if (lEmpleado == null) {
			System.out.println("No hay empleados disponibles");
			return;
		}

		// empleado.setId(lEmpleado.get(comboBoxEmpleado.getSelectedIndex()).getId());
		// empleado = AppMain.per.buscarEmpleado(empleado);
		empleado = lEmpleado.get(comboBoxEmpleado.getSelectedIndex());

		btnSeleccionarEmpleado.setEnabled(false);
		comboBoxEmpleado.setEnabled(false);
		empleadoSeleccionado = true;
		AppMain.per.transaccionRefresh(empleado);
		DefaultTableModel model2 = (DefaultTableModel) tableTratamientos.getModel();// borro tabla
		model2.setRowCount(0);
		System.out.println("empleado id " + empleado.getId());
	}

	protected void seleccionarAnimal() {
		if (tableAnimal.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(this, "No has seleccionado ningun animal");
			return;
		}

		// actualizo formulario
		textFieldNombre.setText(alAnimales.get(tableAnimal.getSelectedRow()).getNombre());
		textFieldID.setText(String.valueOf((alAnimales.get(tableAnimal.getSelectedRow()).getId())));
		// pongo a false lo del animal
		textFieldNombre.setEnabled(false);
		btnBuscarAnimal.setEnabled(false);
		btnSeleccionarAnimal.setEnabled(false);
		comboBoxEspecie.setEnabled(false);
		tableAnimal.setEnabled(false);
		// obtengo el animal
		animal.setId(Integer.valueOf(textFieldID.getText()));
		animal = AppMain.per.buscarAnimal(animal);
		animalSeleccionado = true;
		AppMain.per.transaccionRefresh(animal);

		DefaultTableModel model2 = (DefaultTableModel) tableTratamientos.getModel();// borro tabla
		model2.setRowCount(0);
		System.out.println("empleado id " + animal.getId());
	}

	protected void borrar() {

		if (tableTratamientos.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(this, "No ha seleccionado ningun tratamiento asociado para borrar.");
			return;
		}

		Animaltratamiento animtrat = lAnimaltratamiento.get(tableTratamientos.getSelectedRow());
		lAnimaltratamientoBorrar.add(animtrat);
		lAnimaltratamiento.remove(animtrat);

		actualizarTablaTratamientos();

		////
		// if (tableTratamientos.getSelectedRow() == -1) {
		// JOptionPane.showMessageDialog(this, "No ha seleccionado ningun consumo para
		// borrar.");
		// return;
		// }
		//
		// Consume cons = lConsume.get(tableTratamientos.getSelectedRow());
		// lConsumeBorrar.add(cons);
		// lConsume.remove(cons);
		// // buscarConsumos();
		//
		// actualizarTablaTratamientos();
	}

	protected void guardar() {

		if (textFieldFecha.getText().length() == 0) {
			JOptionPane.showMessageDialog(this, "El campo de cantidad no puede estar vacio.");
			return;
		}

		Date fecha = new Date();

		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			fecha = dateFormat.parse(textFieldFecha.getText());
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(this, "El formato de la fecha debe ser: dd/MM/yyyy HH:mm");
			e.printStackTrace();
			return;
		}

		AnimaltratamientoId animaltratamientoId = new AnimaltratamientoId();
		animaltratamientoId.setIdAnimal(animal.getId());
		animaltratamientoId.setIdEmpleado(empleado.getId());
		animaltratamientoId.setIdTratamiento(tratamiento.getId());
		animaltratamientoId.setFechaHora(fecha);

		Animaltratamiento animtrat = new Animaltratamiento(animaltratamientoId, animal, empleado, tratamiento);
		lAnimaltratamiento.add(animtrat);

		actualizarTablaTratamientos();
		btnBorrar.setEnabled(false);

		textFieldAlimentoGuardar.setVisible(false);
		textFieldFecha.setVisible(false);
		lblTratamientoInv.setVisible(false);
		lblFechaInv.setVisible(false);
		btnGuardar.setVisible(false);

		textFieldAlimentoGuardar.setEnabled(false);
		textFieldFecha.setEnabled(false);
		lblTratamientoInv.setEnabled(false);
		lblFechaInv.setEnabled(false);
		btnGuardar.setEnabled(false);

	}

	protected void buscarAnimal() {
		aniadirTablaAnimal();

	}

	private void aniadirTablaAnimal() {
		alAnimales = AppMain.per.extraerAnimal(textFieldNombre.getText().toUpperCase().trim(),
				comboBoxEspecie.getSelectedItem().toString());

		System.out.println(alAnimales.size());
		DefaultTableModel model = (DefaultTableModel) tableAnimal.getModel();// borro tabla
		model.setRowCount(0);

		for (int i = 0; i < alAnimales.size(); i++) {
			Object[] f = new Object[5];
			f[0] = alAnimales.get(i).getId();// añado id
			f[1] = alAnimales.get(i).getNombre();// añado descripcion
			f[2] = alAnimales.get(i).getEspecie().getDescripcion();
			f[3] = alAnimales.get(i).getZona().getDescripcion();

			Date date = alAnimales.get(i).getFechaNac();
			String strDate = "";
			if (date != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				strDate = dateFormat.format(date);
			}
			f[4] = strDate;
			model.addRow(f);// lo inserto en tabla
		}

	}

	private void inicio() {

		animal = new Animal();
		tratamiento = new Tratamiento();
		empleado = new Empleado();

		lAnimaltratamientoBorrar = new ArrayList<>();
		lAnimaltratamiento = new ArrayList<>();

		animalSeleccionado = false;
		tratamientoSeleccionado = false;
		empleadoSeleccionado = false;
		// items
		tableAnimal.setEnabled(true);
		btnSeleccionarAnimal.setEnabled(true);
		btnBuscarAnimal.setEnabled(true);
		btnSeleccionarTratamiento.setEnabled(true);
		btnSeleccionarEmpleado.setEnabled(true);
		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);
		// textFields
		textFieldID.setText("");
		textFieldNombre.setEnabled(true);
		textFieldNombre.setText("");
		textFieldAlimentoGuardar.setText("");
		textFieldFecha.setText("");
		comboBoxEspecie.setEnabled(true);
		comboBoxTratamiento.setEnabled(true);
		comboBoxEmpleado.setEnabled(true);
		// invisibles
		textFieldAlimentoGuardar.setVisible(false);
		textFieldFecha.setVisible(false);
		lblTratamientoInv.setVisible(false);
		lblFechaInv.setVisible(false);
		btnGuardar.setVisible(false);

		DefaultTableModel model = (DefaultTableModel) tableAnimal.getModel();// borro tabla
		model.setRowCount(0);
		DefaultTableModel model2 = (DefaultTableModel) tableTratamientos.getModel();// borro tabla
		model2.setRowCount(0);

		btnBorrar.setEnabled(false);
		btnGuardar.setEnabled(false);

	}
}
